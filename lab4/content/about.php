<?php
    $title = 'Узнать размер';
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    $scripts = array();
    // определение корня проекта
    $path_root = getcwd().'/';

    function getFileSize($path)
    {
        $fileSize = 0;
        if (is_dir($path)) {
            $dir = scandir($path);
            foreach($dir as $file)
            {
                if (($file!='.') && ($file!='..'))
                    if(is_dir($path . '/' . $file))
                        $fileSize += getFileSize($path.'/'.$file);
                    else
                        $fileSize += filesize($path . '/' . $file);
            }
        } else {
            $fileSize += filesize($path);
        }

        return $fileSize;
    }

    // Определение размера файла/папки
    $size = false;
    if (isset($_GET['btn_ok'])) {
        // не выходит ли путь из директории
        $index = strpos($_GET['filename'], '..');

        if (gettype($index) == 'boolean' and $index == false) {
            $file_path = $path_root.$_GET['filename'];
            $file_path = realpath($file_path);
            if ($file_path) {
                $size = getFileSize($file_path);
            }
        }
    }

    require_once $dir . 'components/header.php';
?>

<main class="register">
    <form class="container row-padding register_block mt-5 mb-5">
        <h2 class="text-center">Определение размера файла</h2>
        <div class="row mb-4 mt-4">
            <div class="col-5 text-right">
                <p class="mb-0 mt-0">Имя файла: </p>
            </div>
            <div class="col-7">
                <input type="text" name="filename" value="<?=$_GET['filename']?>">
            </div>
        </div>
        <div class="row mb-4 mt-4">
            <div class="col-12">
                <?php if (gettype($size) != 'boolean') { 
                    $fileSize = sprintf('Путь: %s <br/>Размер: %d байт.', $file_path, $size);
                ?>
                    <p class="text-correct mt-0 mb-0 pos-relative"><?= $fileSize ?></p>
                <?php } else { ?>
                    <p class="text-error text-center mt-0 mb-0 pos-relative">Нет такого файла или каталога.</p>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <input class="btn-register" type="submit" name="btn_ok" value="Получить размер">
            </div>
        </div>
    </form>
</main>

<?php
    require_once $dir . 'components/footer.php';
?>