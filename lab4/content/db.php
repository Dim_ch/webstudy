<?php
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    $scripts = array();
    $user_id;
    $action;
    $action_back;
    $url_back;
    $view;
    $id;
    $plane;
    $planes;
    $isInfo;

    $urls = array(
        'list' => '/content/db.php?action=list',
        'create' => '/content/db.php?action=create',
        'edit' => '/content/db.php?action=edit',
        'delete' => '/content/db.php?action=delete',
        'view' => '/content/db.php?action=view',
        'back' => '/content/db.php'
    );

    require_once $dir . "controllers/watchCookie.php";

    if (!$isAuth) {
        $_SESSION['message'] = "Войдите в аккаунт";
        header('Location: /content/login.php');
        exit;
    }

    if (!isset($_SESSION['user_id'])) {
        $_SESSION['message'] = "что-то пошло не так";
        header('Location: /content/login.php');
        exit;
    }
    $user_id = $_SESSION['user_id'];

    require_once $dir . 'admin/modulePlane.php';
    require_once $dir . 'components/header.php';
?>

<main class="container">
    <?php
        require_once $dir."components/showMessage.php";
        require_once $dir.$view;
    ?>
</main>

<?php
    require_once $dir . 'components/footer.php'
?>

</body>
</html>