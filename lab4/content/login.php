<?php
    $title = 'Вход';
    $scripts = array();
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    
    require_once $dir . 'core/DataBase.php';
    require_once $dir . 'models/User.php';
    
    $validPassword = false;
    $validPassword = false;
    
    $isPost = $_SERVER['REQUEST_METHOD'] === 'POST';
    
    if($isPost) {
        if (isset($_POST['btn_ok'])) {
            
            if (isset($_POST['username'])) {
                $validUserName = preg_match('|^[a-z_+.*0-9\/]{3,128}$|ui', $_POST['username'], $matches,
                PREG_OFFSET_CAPTURE);
            }
            
            if (isset($_POST['password'])) {
                $validPassword = preg_match('|.{6,20}$|i', $_POST['password'], $matches,
                PREG_OFFSET_CAPTURE);
            }
            
            if ($validPassword && $validUserName) {
                $db = new DataBase();
                $db->connect();
                
                if (!$db->isReady()) {
                    echo "Ошибка на сервере";
                    exit;
                }
                
                $password = hash('sha256', $_POST['password']);

                $username = $_POST['username'];
                
                $user = new User();
                
                $user->setDbConnection($db);

                session_start();
                
                if ($user->get('username', $username)) {
                    if (strcmp($password, $user->password) == 0) {
                        // ==== Вход выполнен ====
                        $_SESSION['username'] = $user->username;
                        $_SESSION['user_role'] = $user->role;
                        $_SESSION['user_id'] = $user->id;
                        $_SESSION['message'] = "Добро пожаловать $user->last_name $user->first_name";
                        header('Location: /index.php');
                        exit;
                    } else {
                        $_SESSION['message'] = "Неверный пароль";
                    }
                } else {
                    $_SESSION['message'] = $user->error;
                }
            }
        }
    }

    require_once $dir . 'components/header.php';
?>

<main class="register">
    <form class="container row-padding register_block mt-5 mb-5" method="POST">
        <h2 class="text-center"><?= $title ?></h2>

        <?php require_once $dir . 'components/showMessage.php' ?>
        
        <div class="divider_footer"></div>
        <div class="row mb-4 mt-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Логин: </p>
            </div>
            <div class="col-8">
                <input type="text" name="username"
                    value="<?= isset($_POST['username']) ? $_POST['username']: ""; 
                        unset($_POST['username']);?>">

                <?php if ($isPost) { ?>
                    <?php if (!$validUserName): ?>
                        <p class="text-error register_hint mt-0 mb-0">
                            Логин должен состоять минимум из 3 букв, может
                            содержать буквы только латинского алфавита, символы
                            "_.*+", цифры и не содержать пробелов.<br>
                        </p>
                    <?php endif; ?>
                <?php }?>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Пароль: </p>
            </div>
            <div class="col-8">
                <input type="password" name="password">

                <?php if ($isPost) { ?>
                    <?php if (!$validPassword): ?>
                        <p class="text-error register_hint mt-0 mb-0">
                            Пароль должен содержать от 6 до 20 символов.
                        </p>
                    <?php endif; ?>
                <?php }?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <input class="btn-register" type="submit" name="btn_ok" value="Войти">
            </div>
        </div>
    </form>
</main>

<?php
    require_once $dir . 'components/footer.php'
?>

</body>
</html>