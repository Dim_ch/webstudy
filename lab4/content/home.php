<?php
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    $scripts = array();
    $action;
    $action_back;
    $url_back;
    $view;
    $id;
    $article;
    $articles;
    $slides;
    
    $urls = array(
        'list' => 'index.php?action=list',
        'view' => 'index.php?action=view',
        'back' => 'index.php'
    );

    require_once $dir . "controllers/watchCookie.php";
    require_once $dir . "core/DataBase.php";
    require_once $dir . "models/Article.php";
    require_once $dir . "models/Slide.php";

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    } else $action = 'list';

    switch($action) {
        case 'list':
            $title = 'Новости';
            $view = 'components/newsList.php';

            $db = new DataBase();
            $db->connect();
        
            if (!$db->isReady()) {
                echo "Ошибка на сервере";
                exit;
            }
        
            $slide = new Slide();
            $slide->setDbConnection($db);
            $slides = $slide->all('active', 1);

            $article = new Article();
            $article->setDbConnection($db);
            $articles = $article->all();
            break;
        case 'view':
            $title = 'Просмотр статьи';
            
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/articleView.php";

                $url_back = $urls['back'];
                $action_back = 'list';

                $db = new DataBase();
                $db->connect();

                if (!$db->isReady()) {
                    echo "Ошибка на сервере";
                    exit;
                }

                $article = new Article();
                $article->setDbConnection($db);

                if ($article->get('id', $id)) {
                    $articles[] = (array) $article;
                } else {
                    echo $article->error;
                    exit;
                }
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
        break;
    }
?>

<main class="container">
    <?php
        require_once $dir.'components/showMessage.php';

        if (strcmp($action, "list") == 0) {
            $scripts[] = "/js/slider.js";
            include_once $dir."components/slider.php";
            echo "<div class='divider'></div>";
        }

        require_once $dir.$view;
    ?>
    <div class="divider"></div>
</main>