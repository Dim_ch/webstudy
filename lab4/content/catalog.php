<?php
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    $scripts = array(
        '/js/filter.js'
    );
    $action;
    $action_back;
    $url_back;
    $view;
    $id;
    $plane;
    $planes;
    $isInfo = true;
    
    $urls = array(
        'list' => '/content/catalog.php?action=list',
        'view' => '/content/catalog.php?action=view',
        'back' => '/content/catalog.php'
    );

    require_once $dir . "controllers/watchCookie.php";
    require_once $dir . "core/DataBase.php";
    require_once $dir . "models/Plane.php";

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    } else $action = 'list';

    switch($action) {
        case 'list':
            $title = 'Каталог';
            $view = 'components/catalogList.php';

            $db = new DataBase();
            $db->connect();
        
            if (!$db->isReady()) {
                echo "Ошибка на сервере";
                exit;
            }

            $plane = new Plane();
            $plane->setDbConnection($db);
            $planes = $plane->all();
            break;
        case 'view':
            $title = 'Просмотр самолета';
            
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/planeView.php";

                $url_back = $urls['back'];
                $action_back = 'list';

                $db = new DataBase();
                $db->connect();

                if (!$db->isReady()) {
                    echo "Ошибка на сервере";
                    exit;
                }

                $plane = new Plane();
                $plane->setDbConnection($db);

                if ($plane->get('id', $id)) {
                    $planes[] = (array) $plane;
                } else {
                    echo $plane->error;
                    exit;
                }
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
        break;
    }

    require_once $dir . 'components/header.php';
?>

<main class="container">
    <div class="divider"></div>
    <?php
        require_once $dir.$view;
    ?>
    <div class="divider"></div>
</main>

<?php
    require_once $dir . 'components/footer.php'
?>

</body>
</html>