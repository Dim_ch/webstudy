<?php 
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';

    require_once $dir . "controllers/watchCookie.php";
    require_once $dir . 'controllers/UploadImage.php';
    require_once $dir . "models/Article.php";
    require_once $dir . 'core/DataBase.php';

    unset($_SESSION['message']);

    if (!$isAdmin) {
        $_SESSION['message'] = "Войдите как администратор";
        header('Location: /content/login.php');
        exit;
    }

    $id;
    $action;
    $module;
    $url_back;
    $action_back;
    $image_old;

    if (isset($_REQUEST['action'])) {
        $action = $_REQUEST['action'];
    } else {
        echo "Ошибка сервера";
        exit;
    }

    if (isset($_REQUEST['module'])) {
        $module = $_REQUEST['module'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    if (strcmp($action, 'create') != 0) {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
        } else {
            echo "Ошибка сервера";
            exit;
        }
    }
    
    if (isset($_REQUEST['url_back'])) {
        $url_back = $_REQUEST['url_back'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    if (isset($_REQUEST['action_back'])) {
        $action_back = $_REQUEST['action_back'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    $db = new DataBase();
    $db->connect();
    
    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $article = new Article();
    $article->setDbConnection($db);

    if (isset($_REQUEST['head'])) {
        $article->head = $_REQUEST['head'];
    }
    else $article->head = "";

    if (isset($_REQUEST['article'])) {
        $article->article = $_REQUEST['article'];
    }
    else $article->article = NULL;

    if (isset($_REQUEST['image_old'])) {
        $image_old = $_REQUEST['image_old'];
        $article->image = $image_old;
    }
    else $article->image = NULL;

    $image = !empty($_FILES['image']['name'])
        ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES['image']['name']) : NULL;

    switch($action) {
        case 'create':
            $errorUpload = "";

            if (!empty($image)) {
                $errorUpload = UploadImage($image);

                if (empty($errorUpload)) {
                    $article->image = "image/uploads/".$image;
                } else {
                    $_SESSION['message'] = $errorUpload;
                }
            }

            if (!$article->insert()) {
                $_SESSION['message'] .= $article->error;
            }

            break;
        case 'edit':
            $article->id = $id;
            
            if (!empty($image)) {

                if (!empty($image_old)) {
                    $target_file = $dir.$image_old;
                    if (file_exists($target_file)) {
                        unlink($target_file);
                    }
                }
                
                $errorUpload = UploadImage($image);
                
                if (!empty($errorUpload)) {
                    $_SESSION['message'] = $errorUpload;
                    header("Location: $url_back?action=edit&id=$id");
                    exit;
                }

                $article->image = "image/uploads/".$image;
            }

            if (!$article->update()) {
                $_SESSION['message'] .= $article->error;
            }

            break;
        case 'delete':
            $article->get('id', $id);
    
            if (!empty($article->image)) {
                unlink($dir.$article->image);
            }

            if (!$article->delete()) {
                $_SESSION['message'] = $article->error;
            }
            break;
    }

    header("Location: $url_back?action=$action_back&id=$id&module=$module");
?>