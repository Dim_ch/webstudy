<?php
    function UploadImage($image) {
        $dir = $_SERVER['DOCUMENT_ROOT'].'/';

        $result_message="";

        // если изображение не пустое, пробуем загрузить его 
        if ($image) {
    
            // функция sha1_file() используется для создания уникального имени файла 
            $target_directory = $dir."image/uploads/";
            $target_file = $target_directory . $image;
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            // сообщение об ошибке пусто 
            $file_upload_error_messages = "";

            // убеждаемся, что файл - изображение 
            $check = getimagesize($_FILES["image"]["tmp_name"]);

            if ($check !== false) {
                // отправленный файл является изображением 
            } else {
                $file_upload_error_messages .= "<div>Отправленный файл не является изображением.</div>";
            }

            // проверяем, разрешены ли определенные типы файлов 
            $allowed_file_types = array("jpg", "jpeg", "png", "gif");

            if (!in_array($file_type, $allowed_file_types)) {
                $file_upload_error_messages .= "<div>Разрешены только файлы JPG, JPEG, PNG, GIF.</div>";
            }

            // убеждаемся, что файл не существует 
            if (file_exists($target_file)) {
                $file_upload_error_messages .= "<div>Изображение уже существует. Попробуйте изменить имя файла.</div>";
            }

            // убедимся, что отправленный файл не слишком большой (не может быть больше 1 МБ) 
            if ($_FILES['image']['size'] > (1024000)) {
                $file_upload_error_messages .= "<div>Размер изображения не должен превышать 1 МБ.</div>";
            }

            // убедимся, что папка uploads существует, если нет, то создаём 
            if (!is_dir($target_directory)) {
                mkdir($target_directory, 0777, true);
            }

            // если $file_upload_error_messages всё ещё пуст 
            if (empty($file_upload_error_messages)) {

                // ошибок нет, пробуем загрузить файл 
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    // фото загружено 
                } else {
                    $result_message .= "<div class='alert alert-danger'>";
                        $result_message .= "<div>Невозможно загрузить фото.</div>";
                        // $result_message .= "<div>Обновите запись, чтобы загрузить фото снова.</div>";
                    $result_message .= "</div>";
                }
            }

            // если $file_upload_error_messages НЕ пусто 
            else {

                // это означает, что есть ошибки, поэтому покажем их пользователю 
                $result_message .= "<div class='alert alert-danger'>";
                    $result_message .= "{$file_upload_error_messages}";
                    // $result_message .= "<div>Обновите запись, чтобы загрузить фото.</div>";
                $result_message .= "</div>";
            }
        }
    
        return $result_message;
    }
?>