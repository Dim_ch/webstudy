<?php 
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';

    require_once $dir . "controllers/watchCookie.php";
    require_once $dir . 'controllers/UploadImage.php';
    require_once $dir . "models/Plane.php";
    require_once $dir . 'core/DataBase.php';

    unset($_SESSION['message']);

    if (!$isAuth) {
        $_SESSION['message'] = "Войдите в аккаунт";
        header('Location: /content/login.php');
        exit;
    }

    $id;
    $action;
    $module;
    $url_back;
    $action_back;
    $image_old;

    if (isset($_REQUEST['action'])) {
        $action = $_REQUEST['action'];
    } else {
        echo "Ошибка сервера";
        exit;
    }

    if (isset($_REQUEST['module'])) {
        $module = $_REQUEST['module'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    if (strcmp($action, 'create') != 0) {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
        } else {
            echo "Ошибка сервера";
            exit;
        }
    }
    
    if (isset($_REQUEST['url_back'])) {
        $url_back = $_REQUEST['url_back'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    if (isset($_REQUEST['action_back'])) {
        $action_back = $_REQUEST['action_back'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    $db = new DataBase();
    $db->connect();
    
    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $plane = new Plane();
    $plane->setDbConnection($db);

    if (isset($_REQUEST['name'])) {
        $plane->name = $_REQUEST['name'];
    }
    else $plane->name = "";

    if (isset($_REQUEST['range'])) {
        $plane->range = $_REQUEST['range'];
    }
    else $plane->range = NULL;

    if (isset($_REQUEST['speed'])) {
        $plane->speed = $_REQUEST['speed'];
    }
    else $plane->speed = NULL;

    if (isset($_REQUEST['passengers'])) {
        $plane->passengers = $_REQUEST['passengers'];
    }
    else $plane->passengers = NULL;

    if (isset($_REQUEST['length'])) {
        $plane->length = $_REQUEST['length'];
    }
    else $plane->length = NULL;

    if (isset($_REQUEST['width'])) {
        $plane->width = $_REQUEST['width'];
    }
    else $plane->width = NULL;

    if (isset($_REQUEST['height'])) {
        $plane->height = $_REQUEST['height'];
    }
    else $plane->height = NULL;

    if (isset($_REQUEST['description'])) {
        $plane->description = $_REQUEST['description'];
    }
    else $plane->description = NULL;

    if (isset($_REQUEST['gallery'])) {
        $plane->gallery = 1;
    }
    else $plane->gallery = 0;

    if (isset($_REQUEST['user_id'])) {
        $plane->user_id = $_REQUEST['user_id'];
    }
    else $plane->user_id = $user_id;

    if (isset($_REQUEST['image_old'])) {
        $image_old = $_REQUEST['image_old'];
        $plane->image = $image_old;
    }
    else $plane->image = NULL;

    $image = !empty($_FILES['image']['name'])
        ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES['image']['name']) : NULL;

    switch($action) {
        case 'create':
            $errorUpload = "";

            if (!empty($image)) {
                $errorUpload = UploadImage($image);

                if (empty($errorUpload)) {
                    $plane->image = "image/uploads/".$image;
                } else {
                    $_SESSION['message'] = $errorUpload;
                }
            }

            if (!$plane->insert()) {
                $_SESSION['message'] .= $plane->error;
            }


        break;
        case 'edit':
            if ($user_id != $plane->user_id) {
                if (!$isAdmin) {
                    $_SESSION['message'] = "У вас не достаточно прав для редактирования этой записи";
                    break;
                }
            }

            $plane->id = $id;
            
            if (!empty($image)) {

                if (!empty($image_old)) {
                    $target_file = $dir.$image_old;
                    if (file_exists($target_file)) {
                        unlink($target_file);
                    }
                }
                
                $errorUpload = UploadImage($image);
                
                if (!empty($errorUpload)) {
                    $_SESSION['message'] = $errorUpload;
                    header("Location: $url_back?action=edit&id=$id");
                    exit;
                }

                $plane->image = "image/uploads/".$image;
            }

            if (!$plane->update()) {
                $_SESSION['message'] .= $plane->error;
            }

        break;
        case 'delete':
            if ($user_id == $plane->user_id || $isAdmin) {
                $plane->get('id', $id);
        
                if (!empty($plane->image)) {
                    unlink($dir.$plane->image);
                }
    
                if (!$plane->delete()) {
                    $_SESSION['message'] = $plane->error;
                }
            } else {
                $_SESSION['message'] = "У вас не достаточно прав для удаления этой записи";
            }
            break;
    }

    header("Location: $url_back?action=$action_back&id=$id&module=$module");
?>