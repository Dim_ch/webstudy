<?php 
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';

    require_once $dir . "controllers/watchCookie.php";
    require_once $dir . 'controllers/UploadImage.php';
    require_once $dir . "models/Slide.php";
    require_once $dir . 'core/DataBase.php';

    unset($_SESSION['message']);

    if (!$isAdmin) {
        $_SESSION['message'] = "Войдите как администратор";
        header('Location: /content/login.php');
        exit;
    }

    $id;
    $action;
    $module;
    $url_back;
    $action_back;
    $image_old;

    if (isset($_REQUEST['action'])) {
        $action = $_REQUEST['action'];
    } else {
        echo "Ошибка сервера";
        exit;
    }

    if (isset($_REQUEST['module'])) {
        $module = $_REQUEST['module'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    if (strcmp($action, 'create') != 0) {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
        } else {
            echo "Ошибка сервера";
            exit;
        }
    }
    
    if (isset($_REQUEST['url_back'])) {
        $url_back = $_REQUEST['url_back'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    if (isset($_REQUEST['action_back'])) {
        $action_back = $_REQUEST['action_back'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    $db = new DataBase();
    $db->connect();
    
    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $slide = new Slide();
    $slide->setDbConnection($db);

    if (isset($_REQUEST['head'])) {
        $slide->head = $_REQUEST['head'];
    }
    else $slide->head = "";

    if (isset($_REQUEST['text'])) {
        $slide->text = $_REQUEST['text'];
    }
    else $slide->text = NULL;

    if (isset($_REQUEST['active'])) {
        $slide->active = 1;
    }
    else $slide->active = 0;

    if (isset($_REQUEST['image_old'])) {
        $image_old = $_REQUEST['image_old'];
        $slide->image = $image_old;
    }
    else $slide->image = NULL;

    $image = !empty($_FILES['image']['name'])
        ? sha1_file($_FILES['image']['tmp_name']) . "-" . basename($_FILES['image']['name']) : NULL;

    switch($action) {
        case 'create':
            $errorUpload = "";

            if (!empty($image)) {
                $errorUpload = UploadImage($image);

                if (empty($errorUpload)) {
                    $slide->image = "image/uploads/".$image;
                } else {
                    $_SESSION['message'] = $errorUpload;
                }
            }

            if (!$slide->insert()) {
                $_SESSION['message'] .= $slide->error;
            }

            break;
        case 'edit':
            $slide->id = $id;
            
            if (!empty($image)) {

                if (!empty($image_old)) {
                    $target_file = $dir.$image_old;
                    if (file_exists($target_file)) {
                        unlink($target_file);
                    }
                }
                
                $errorUpload = UploadImage($image);
                
                if (!empty($errorUpload)) {
                    $_SESSION['message'] = $errorUpload;
                    header("Location: $url_back?action=edit&id=$id");
                    exit;
                }

                $slide->image = "image/uploads/".$image;
            }

            if (!$slide->update()) {
                $_SESSION['message'] .= $slide->error;
            }

            break;
        case 'delete':
            $slide->get('id', $id);
    
            if (!empty($slide->image)) {
                unlink($dir.$slide->image);
            }

            if (!$slide->delete()) {
                $_SESSION['message'] = $slide->error;
            }
            break;
    }

    header("Location: $url_back?action=$action_back&id=$id&module=$module");
?>