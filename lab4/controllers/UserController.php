<?php 
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';

    require_once $dir . "controllers/watchCookie.php";
    require_once $dir . "models/User.php";
    require_once $dir . 'core/DataBase.php';

    unset($_SESSION['message']);

    
    if (!$isAdmin) {
        $_SESSION['message'] = "Войдите как администратор";
        header('Location: /content/login.php');
        exit;
    }
    
    $id;
    $action;
    $module;
    $url_back;
    $action_back;
    $image_old;

    $validFirstName = false;
    $validLastName = false;
    $validUserName = false;
    $validPassword = false;

    if (isset($_REQUEST['action'])) {
        $action = $_REQUEST['action'];
    } else {
        echo "Ошибка сервера";
        exit;
    }

    if (isset($_REQUEST['module'])) {
        $module = $_REQUEST['module'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    if (strcmp($action, 'create') != 0) {
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
        } else {
            echo "Ошибка сервера";
            exit;
        }
    }
    
    if (isset($_REQUEST['url_back'])) {
        $url_back = $_REQUEST['url_back'];
    } else {
        echo "Ошибка сервера";
        exit;
    }
    
    if (isset($_REQUEST['action_back'])) {
        $action_back = $_REQUEST['action_back'];
    } else {
        echo "Ошибка сервера";
        exit;
    }

    if (strcmp($action, 'delete') != 0) {
        if (isset($_REQUEST['first_name'])) {
            $validFirstName = preg_match('|^[А-Я][а-я]{2,128}$|u', $_REQUEST['first_name'], $matches,
            PREG_OFFSET_CAPTURE);
            
            if (!$validFirstName) {
                $_SESSION['message'] .= "Имя должно состоять минимум из 3 букв,
                содержать буквы русского алфавита и не содержать пробелов.<br>
                Первая буква должна быть прописной.<br>";
            }
        }
        
        if (isset($_REQUEST['last_name'])) {
            $validLastName = preg_match('|^[А-Я][а-я]{2,128}$|u', $_REQUEST['last_name'], $matches,
            PREG_OFFSET_CAPTURE);
    
            if (!$validLastName) {
                $_SESSION['message'] .= "Фамилия должна состоять минимум из 3 букв,
                содержать буквы русского алфавита и не содержать пробелов.<br>
                Первая буква должна быть прописной.<br>";
            }
        }
        
        if (isset($_REQUEST['username'])) {
            $validUserName = preg_match('|^[a-z_+.*0-9\/]{3,128}$|ui', $_REQUEST['username'], $matches,
            PREG_OFFSET_CAPTURE);
    
            if (!$validUserName) {
                $_SESSION['message'] .= 'Логин должен состоять минимум из 3 букв, может
                содержать буквы только латинского алфавита, символы
                "_.*+", цифры и не содержать пробелов.<br>';
            }
        }
        
        if (isset($_REQUEST['password'])) {
            $validPassword = preg_match('|^[A-Z0-9\\\/*-+.,]{6,20}$|i', $_REQUEST['password'], $matches,
            PREG_OFFSET_CAPTURE);
    
            if (!$validPassword) {
                $_SESSION['message'] .= 'Пароль должен состоять из букв латинского алфавита, цифр, и символов "/\*-+.,".<br>
                Первый символ пароля должен быть латинской буквой или цифрой.<br>
                Длина пароля должна быть не меньше 6 символов.<br>';
            }
        }
    }
    
    if (($validFirstName && $validLastName && $validPassword && $validUserName) || strcmp($action, 'delete') == 0) {
        $db = new DataBase();
        $db->connect();
        
        if (!$db->isReady()) {
            echo "Ошибка на сервере";
            exit;
        }
        
        $password = hash('sha256', $_REQUEST['password']);
        
        $user = new User();

        $user->setDbConnection($db);
        
        $user->first_name = $_REQUEST['first_name'];
        $user->last_name = $_REQUEST['last_name'];
        $user->username = $_REQUEST['username'];
        $user->password = $password;
        $user->email = (!empty($_REQUEST['email'])) ? $_REQUEST['email'] : NULL;
        $user->date_registration = isset($_REQUEST['date_registration']) ? $_REQUEST['date_registration'] : date('Y-m-d');
        $user->role = $_REQUEST['role'];

        switch($action) {
            case 'create':
                if (!$user->insert()) {
                    $_SESSION['first_name'] = $user->first_name;
                    $_SESSION['last_name'] = $user->last_name;
                    $_SESSION['username'] = $user->username;
                    $_SESSION['email'] = $user->email;

                    $_SESSION['message'] = $user->error;
                }
            break;
            case 'edit':
                $user->id = $id;
                
                if (!$user->update()) {
                    $_SESSION['message'] = $user->error;
                }
            break;
            case 'delete':

                $user->id = $id;

                if ($user->id != $user_id) {
                    if (!$user->delete()) {
                        $_SESSION['message'] = $user->error;
                    }

                }
            break;
        }
    } else {
        $_SESSION['first_name'] = $_REQUEST['first_name'];
        $_SESSION['last_name'] = $_REQUEST['last_name'];
        $_SESSION['username'] = $_REQUEST['username'];
        $_SESSION['email'] = $_REQUEST['email'];
        $action_back = $action;
    }

    $id = (isset($id)) ? "&id=$id" : '';
    header("Location: $url_back?action=$action_back$id&module=$module");
?>