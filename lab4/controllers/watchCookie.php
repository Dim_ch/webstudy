<?php
    session_start();
    
    $isAuth = isset($_SESSION['user_role']);
    $isAdmin = false;
    $user_id;
    $username;

    if ($isAuth) {

        if (isset($_SESSION['user_role'])) {
            $isAdmin = (int)$_SESSION['user_role'] == 1;
        } else {
            $_SESSION['message'] = "Войдите";
            header("Location: /content/login.php");
            exit;
        }

        if (isset($_SESSION['user_id'])) {
            $user_id = $_SESSION['user_id'];
        } else {
            $_SESSION['message'] = "Войдите";
            header("Location: /content/login.php");
            exit;
        }

        if (isset($_SESSION['username'])) {
            $username = $_SESSION['username'];
        } else {
            $_SESSION['message'] = "Войдите";
            header("Location: /content/login.php");
            exit;
        }
    }
?>