<?php 
    class DataBase {
        private $host = 'localhost';

        private $db_name = 'lab4_planes';

        private $username = 'dimje';

        private $password = 'dimje';

        private $connection;

        private $is_ready = false;

        private $error_message = "";

        public function connect() {
            $this->connection = new mysqli($this->host,
                                           $this->username,
                                           $this->password,
                                           $this->db_name,
                                           3306
                                        );
    
            $this->is_ready = !(bool)$this->connection->connect_errno;
    
            if (!$this->is_ready) {
                $error_message = "Не удалось подключиться к MySQL: (" . 
                                $this->connection->connect_errno . ") " . 
                                $this->connection->connect_error;
            }
        }

        public function getConnection() {
            return $this->connection;
        }

        public function isReady(): bool {
            return $this->is_ready;
        }

        public function getErrorMessage() {
            return $this->error_message;
        }
    }
?>