<?php 
    $count = count($slides);
?>

<div class="row slider">
    <ul class="slick">
        <?php
            for ($i = 0; $i < $count; $i++) {
                echo '<li></li>';
            }
        ?>
    </ul>

    <?php
        foreach ($slides as $item) {
            $head = $item['head'];
            $image = $item['image'];
            $text = $item['text'];

            if ($item['active']) {
                echo '<div class="slider_item">';
                    echo "<h1>$head</h1>";
                    echo "<img src='/$image' alt='$head'>";
                    echo "<p>$text</p>";
                echo '</div>';
            }
        }
    ?>

    <a href="#" class="next"><i class="right"></i></a>
    <a href="#" class="prev"><i class="left"></i></a>
</div>