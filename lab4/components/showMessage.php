<?php
    if (isset($_SESSION['message'])) {

        echo "<div class='row row-padding mb-4 mt-4'>";
        echo "<p class='text-info mt-0 mb-0'>";
        echo $_SESSION['message'];
        echo "</p></div>";

        unset($_SESSION['message']);
    }
?>