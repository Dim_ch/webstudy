<?php
    $scripts[] = '/js/gallery.js';
?>

<div class="row row-padding gallery">
    <div class="col-12 col-md-5 col-lg-4 col-xl-3 gallery_list">
        <div class="gallery_list_img active">
            <img src="../image/cesna150.jpg" alt="cessna150">
        </div>
        <div class="gallery_list_img">
            <img src="../image/cessna_150_cockpit.jpg" alt="cessna_150_cockpit">
        </div>
        <div class="gallery_list_img">
            <img src="../image/cessna_150_fly.jpg" alt="cessna_150_fly">
        </div>
        <div class="gallery_list_img">
            <img src="../image/cessna_150_fly.jpg" alt="cessna_150_fly">
        </div>
    </div>

    <div class="col-12 col-md-7 col-lg-8 col-xl-9 gallery_view">
        <div class="gallery_view_block">
            <img class="article_main_img active_img" src="../image/cesna150.jpg" alt="cessna150">
        </div>
    </div>
</div>