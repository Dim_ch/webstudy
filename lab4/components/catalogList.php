<div class='row justify-content-center'>
        <!-- Каталог -->
        <div class='col-12 col-md-9 col-xl-10 order-sm-2'>
        <?php
            echo "<div class='row catalog'>";
            foreach($planes as $cur_plane) {
                echo "<div class='col-6 col-md-4 col-lg-3 mb-4'>";
                    echo "<div class='catalog_img'>";
                        echo "<img class='article_main_img' src='/".$cur_plane['image']."' alt='".$cur_plane['name']."'>";
                    echo "</div>";
                    echo "<div class='news_content p-2'>";
                        echo "<h3 class='mb-0 mt-0'>".$cur_plane['name']."</h3>";
                        echo "<a href='".$urls['view']."&id=".$cur_plane['id']."'>Перейти</a>";
                    echo "</div>";
                echo "</div>";
            }
            echo "</div>";
        ?>
        </div>
</div>