<?php
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';

    require_once $dir . "controllers/watchCookie.php";

    header("Content-type: text/html;charset=utf-8");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" type="iamge/x-icon" href="/image/favicon.ico">
    <title>
        <?php echo $title; ?>
    </title>
</head>
<body>
    <div class="size-window">
        <p></p>
    </div>
    <header class="container navigation">
        <nav class="row">
            <button class="btn-list">+</button>
            <ul>
                <li><a href="/index.php">Главная</a></li>
                <li><a href="/content/catalog.php">Каталог</a></li>
                <?php
                    if ($isAuth) {
                        echo "<li><a href='/content/db.php?action=list'>База данных</a></li>";
                        if ($isAdmin) {
                            echo '<li><a href="/admin/index.php">Админка</a></li>';
                        }
                        echo '<li><a href="/controllers/logout.php">Выход</a></li>';
                    } else {
                        echo '<li><a href="/content/register.php">Регистрация</a></li>';
                        echo '<li><a href="/content/login.php">Вход</a></li>';
                    }
                ?>
            </ul>
        </nav>
        <div class="list-menu-container">
            <div class="list-menu-items">
                <ul class="list-menu-item-container text-center">
                    <li>
                        <a href="/index.php">Главная</a>
                    </li>
                    <li>
                        <a href="/content/catalog.php">Каталог</a>
                    </li>
                    <li>
                        <a href="/content/about.php">Размеры файлов</a>
                    </li>
                    <?php
                        if ($isAuth) {
                            if ($isAdmin) {
                                echo '<li><a href="/admin/index.php">Админка</a></li>';
                            }
                            echo '<li><a href="/controllers/logout.php">Выход</a></li>';
                        } else {
                            echo '<li><a href="/content/register.php">Регистрация</a></li>';
                            echo '<li><a href="/content/login.php">Вход</a></li>';
                        }
                    ?>
                </ul>
            </div>
            <div class="list-menu-empty"></div>
        </div>
    </header>
