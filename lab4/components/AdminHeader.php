<?php
    header("Content-type: text/html;charset=utf-8");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" type="iamge/x-icon" href="/image/favicon.ico">
    <title>
        <?php echo $title; ?>
    </title>
</head>