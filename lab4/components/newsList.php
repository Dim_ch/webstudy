<h1 class='text-center mt-4 mb-4 news_title'>
    Новости
    <span class='tooltip'>hello</span>
</h1>

<div class='row news pb-4 mt-5'>
<?php
    $count = count($news);
    foreach ($articles as $cur_article) {
        echo "<div class='col-12 col-xl-6 mb-4'>";
            echo "<div class='news_img'>";
                echo "<img src='/".$cur_article['image']."' alt='".$cur_article['head']."'>";
            echo "</div>";
            echo "<div class='news_content'>";
                echo "<h3 class='mb-0 text-center'>".$cur_article['head']."</h3>";
                echo "<div  class='news_text mb-3'>";
                    echo "<p>".mb_substr($cur_article['article'], 0, 200, 'utf-8')."...</p>";
                echo "</div>";
                echo "<a href='".$urls['view']."&id=".$cur_article['id']."'>Перейти</a>";
            echo "</div>";
        echo "</div>";
    }
?>
</div>