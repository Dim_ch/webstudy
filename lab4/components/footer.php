<footer class="container footer">
    <div class="row row-padding">
        <div class="col-4 text-center">
            <h2>Сайт</h2>
            <ul>
                <li><a href="/index.php">Главная</a></li>
                <li><a href="/content/catalog.php">Каталог</a></li>
            </ul>
        </div>
        <div class="col-4 text-center">
            <h2>Ссылки</h2>
            <ul>
                <li><a href="/content/about.php">Размеры файлов</a></li>
                <li><a href="#">Ссылка 2</a></li>
                <li><a href="#">Ссылка 2</a></li>
                <li><a href="#">Ссылка 3</a></li>
            </ul>
        </div>
        <div class="col-4 text-center">
            <h2>Контакты</h2>
            <ul>
                <li>Область: Белгородская область</a></li>
                <li>Город: Старый-Оскол</a></li>
                <li>Телефон: 123 456 789</a></li>
            </ul>
        </div>
    </div>
    <div class="divider divider_footer"></div>
    <div class="row">
        <div class="col-12 footer_author">
            <span class="text-center">The site was created by Dmitry Anistratov<br>&#169; 2021</span>
        </div>
    </div>
</footer>

<script src="/js/menu.js"></script>

<?php foreach ($scripts as $script) { ?>
  <script src="<?=$script?>"></script>
<?php }?>