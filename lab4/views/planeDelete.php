<?php
    require_once $dir . "models/Plane.php";
    require_once $dir . 'core/DataBase.php';

    $controller = "/controllers/PlaneController.php?id=$id&url_back="
        .$urls['back']."&action=$action&action_back=list&module=plane";
    $name = "запись";
    $table_name = "таблица не задана";

    $db = new DataBase();
    $db->connect();

    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $plane = new Plane();
    $plane->setDbConnection($db);

    if ($plane->get('id', $id)) {
        $name = $plane->name;
        $table_name = $plane->table_name;
    } else {
        echo $plane->error;
        exit;
    }
?>

<div class="row row-padding">
    <div class="col-1"></div>
    <div class="col-11">
        <?php
            require_once $dir . "views/deleteElement.php";
        ?>
    </div>
</div>