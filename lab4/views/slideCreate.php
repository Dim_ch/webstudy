<?php
    require_once $dir . 'core/DataBase.php';
    require_once $dir . 'models/Slide.php';
    
    $valueButton = "Добавить";
    
    $db = new DataBase();
    $db->connect();
    
    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $slide = new Slide();
    $slide->setDbConnection($db);
    
    $isEdit = isset($_GET['id']);
    if($isEdit) {
        $valueButton = "Редактировать";
        
        if (!$slide->get('id', $_GET['id'])) {
            echo($slide->error);
            exit;
        }
    }
?>

<div class="row register">
    <form
    class="container row-padding register_block mt-5 mb-5"
    action="/controllers/SlideController.php"
    method="POST"
    style="width: 80%"
    enctype="multipart/form-data"
    >

        <div class="col-12">
            <p>
                <a class="register_block__a" href="<?= "$url_back?action=$action_back&id=$id&module=slide" ?>">Назад</a>
            </p>
        </div>
    
        <h2 class="text-center"><?= $title ?></h2>

        <div class="divider_footer"></div>

        <div class="row mb-4 mt-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Заголовок: </p>
            </div>
            <div class="col-8">
                <input type="text" name="head" value="<?= $isEdit ? $slide->head: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Текст: </p>
            </div>
            <div class="col-8">
                <textarea name="text" rows="15"><?= $isEdit ? $slide->text: ''; ?></textarea>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Активный: </p>
            </div>
            <div class="col-8">
            <?php
                $checked = 'checked';

                if ($isEdit) {
                    $checked = ((int)$slide->active == 0) ? '' : $checked;
                }

                echo "<label><input type='checkbox' name='active' $checked/></label>";
            ?>
            </div>
        </div>

        <?php
            if (!empty($slide->image)) {
                echo "<div class='row mb-4'>";
                    echo "<div class='col-4 text-right'>";
                        echo "<p class='mb-0 mt-0'>Текущее изображение: </p>";
                    echo "</div>";
                    echo "<div class='col-8'>";
                        echo "<div class='catalog_img'>";
                        echo "<img class='article_main_img' src='/$slide->image' alt='$slide->head'>";
                        echo "</div>";
                    echo "</div>";
                echo "</div>";
            }
        ?>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Изображение: </p>
            </div>
            <div class="col-8">
                <input type="file" name="image">
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <?php
                    echo "<input class='btn-register' type='submit' name='btn_ok' value='".$valueButton."'>";
                    echo "<input hidden name='action' value='".$action."'>";
                    echo "<input hidden name='action_back' value='$action_back'>";
                    echo "<input hidden name='url_back' value='".$url_back."'>";
                    echo "<input hidden name='module' value='slide'>";

                    if ($isEdit) {
                        echo "<input hidden name='id' value='".$_GET['id']."'>";
                        echo "<input hidden name='image_old' value='".$slide->image."'>";
                    }
                ?>
            </div>
        </div>
    </form>
</div>