<?php 
    $count = count($planes);
?>

<div class="row row-padding">
    <div class="col-12 table-container mb-4">
        <table>
            <tr>
                <?php
                    if(!$isInfo && $isAuth) {
                        echo '<th>';
                        echo '<a href="'.$urls['create'].'">Добавить</a>';
                        echo '</th>';
                    }
                        echo '<th>Модель</th>';
                        echo '<th>Дальность полета<br>(км)</th>';
                        echo '<th>Крейсерская скорость<br>(км/ч)</th>';
                        echo '<th>Максимальное число пассажиров</th>';
                        echo '<th>Длина</th>';
                        echo '<th>Ширина</th>';
                        echo '<th>Высота</th>';

                ?>
            </tr>
            <?php
                foreach($planes as $cur_plane) {
                    echo '<tr>';
                        if(!$isInfo && $isAuth) {
                            echo "<td>";
                            if ($user_id == $cur_plane['user_id'] || $isAdmin) {
                                echo "<a href='".$urls['edit']."&id=".$cur_plane['id']."'>Редактировать</a>";
                                echo "<a href='".$urls['delete']."&id=".$cur_plane['id']."'>Удалить</a>";
                            }
                            echo "<a href='".$urls['view']."&id=".$cur_plane['id']."'>Просмотреть</a>";
                            echo "</td>";
                        }
                            echo "<td>" . $cur_plane['name'] . "</td>";
                            echo "<td>" . $cur_plane['range'] . "</td>";
                            echo "<td>" . $cur_plane['speed'] . "</td>";
                            echo "<td>" . $cur_plane['passengers'] . "</td>";
                            echo "<td>" . $cur_plane['length'] . "</td>";
                            echo "<td>" . $cur_plane['width'] . "</td>";
                            echo "<td>" . $cur_plane['height'] . "</td>";
                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</div>