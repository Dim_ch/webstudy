<?php
    require_once $dir . "models/Article.php";
    require_once $dir . 'core/DataBase.php';

    $controller = "/controllers/ArticleController.php?id=$id&url_back="
        .$urls['back']."&action=$action&action_back=list&module=article";
    $name = "запись";
    $table_name = "таблица не задана";

    $db = new DataBase();
    $db->connect();

    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $article = new Article();
    $article->setDbConnection($db);

    if ($article->get('id', $id)) {
        $name = "статью '$article->head'";
        $table_name = $article->table_name;
    } else {
        echo $article->error;
        exit;
    }
?>

<div class="row row-padding">
    <div class="col-1"></div>
    <div class="col-11">
        <?php
            require_once $dir . "views/deleteElement.php";
        ?>
    </div>
</div>