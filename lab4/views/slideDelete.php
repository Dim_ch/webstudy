<?php
    require_once $dir . "models/Slide.php";
    require_once $dir . 'core/DataBase.php';

    $controller = "/controllers/SlideController.php?id=$id&url_back="
        .$urls['back']."&action=$action&action_back=list&module=slide";
    $name = "запись";
    $table_name = "таблица не задана";

    $db = new DataBase();
    $db->connect();

    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $slide = new Slide();
    $slide->setDbConnection($db);

    if ($slide->get('id', $id)) {
        $name = "слайд '$slide->head'";
        $table_name = $slide->table_name;
    } else {
        echo $slide->error;
        exit;
    }
?>

<div class="row row-padding">
    <div class="col-1"></div>
    <div class="col-11">
        <?php
            require_once $dir . "views/deleteElement.php";
        ?>
    </div>
</div>