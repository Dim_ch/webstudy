<?php 
    $count = count($slides);
?>

<div class="row row-padding">
    <div class="col-12 table-container mb-4">
        <table>
            <tr>
                <?php
                    if($isAdmin) {
                        echo '<th>';
                        echo '<a href="'.$urls['create'].'">Добавить</a>';
                        echo '</th>';
                    }
                    echo '<th>Заголовок</th>';
                    echo '<th>Текст</th>';
                    echo '<th>Активный</th>';
                    echo '<th>Изображение</th>';
                ?>
            </tr>
            <?php
                foreach($slides as $cur_slide) {
                    echo '<tr>';
                        if($isAdmin) {
                            echo "<td>";
                                echo "<a href='".$urls['edit']."&id=".$cur_slide['id']."'>Редактировать</a>";
                                echo "<a href='".$urls['delete']."&id=".$cur_slide['id']."'>Удалить</a>";
                                echo "<a href='".$urls['view']."&id=".$cur_slide['id']."'>Смотреть</a>";
                            echo "</td>";
                        }
                        echo "<td>" . $cur_slide['head'] . "</td>";
                        echo "<td>" . mb_substr($cur_slide['text'], 0, 80, 'utf-8') . "...</td>";
                        echo "<td>" . $cur_slide['active'] . "</td>";
                        echo "<td>" . $cur_slide['image'] . "</td>";
                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</div>