<div class="row slider">
    <?php
        echo "<img class='article_main_img' src='/$plane->image' alt='$plane->name'>";
    ?>
</div>

<div class="divider"></div>

<div class="col-12">
    <p>
        <?php
            echo "<a href='".$urls['list']."'>Назад</a>";
            if($isAuth) {
                if (($user_id == $plane->user_id || $isAdmin) && $isEditable) {
                    echo "<a
                        href='".$urls['edit']."&id=".$plane->id
                        ."&url_back=$url_back&action_back=view'>Редактировать</a>";
                    echo "<a
                        href='".$urls['delete']."&id=".$plane->id
                        ."&url_back=$url_back&action_back=view'>Удалить</a>";
                }
            }
        ?>
    </p>
</div>
<!-- Описание и таблица -->
<div class="row row-padding">
    <!-- Таблица -->
    <div class="col-12 table-container">
        <?php
            require_once $dir."views/planeList.php";
        ?>
    </div>
</div>
<!-- Описание -->
<div class="row row-padding">
    <div class="col-12 article">
        <?php
            echo "<h2 class='text-center'>".$plane->name."</h2>";
            echo "<p>";
                echo $plane->description;
            echo "</p>";
        ?>
    </div>
</div>
<div class="divider"></div>
