<?php
    require_once $dir . 'core/DataBase.php';
    require_once $dir . 'models/Article.php';
    
    $valueButton = "Добавить";
    
    $db = new DataBase();
    $db->connect();
    
    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $article = new Article();
    $article->setDbConnection($db);
    
    $isEdit = isset($_GET['id']);
    if($isEdit) {
        $valueButton = "Редактировать";
        
        if (!$article->get('id', $_GET['id'])) {
            echo($article->error);
            exit;
        }
    }
?>

<div class="row register">
    <form
    class="container row-padding register_block mt-5 mb-5"
    action="/controllers/ArticleController.php"
    method="POST"
    style="width: 80%"
    enctype="multipart/form-data"
    >

        <div class="col-12">
            <p>
                <a class="register_block__a" href="<?= "$url_back?action=$action_back&id=$id&module=article" ?>">Назад</a>
            </p>
        </div>
    
        <h2 class="text-center"><?= $title ?></h2>

        <div class="divider_footer"></div>

        <div class="row mb-4 mt-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Заголовок: </p>
            </div>
            <div class="col-8">
                <input type="text" name="head" value="<?= $isEdit ? $article->head: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Текст: </p>
            </div>
            <div class="col-8">
                <textarea name="article" rows="15"><?= $isEdit ? $article->article: ''; ?></textarea>
            </div>
        </div>

        <?php
            if (!empty($article->image)) {
                echo "<div class='row mb-4'>";
                    echo "<div class='col-4 text-right'>";
                        echo "<p class='mb-0 mt-0'>Текущее изображение: </p>";
                    echo "</div>";
                    echo "<div class='col-8'>";
                        echo "<div class='catalog_img'>";
                        echo "<img class='article_main_img' src='/$article->image' alt='$article->head'>";
                        echo "</div>";
                    echo "</div>";
                echo "</div>";
            }
        ?>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Изображение: </p>
            </div>
            <div class="col-8">
                <input type="file" name="image">
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <?php
                    echo "<input class='btn-register' type='submit' name='btn_ok' value='".$valueButton."'>";
                    echo "<input hidden name='action' value='".$action."'>";
                    echo "<input hidden name='action_back' value='$action_back'>";
                    echo "<input hidden name='url_back' value='".$url_back."'>";
                    echo "<input hidden name='module' value='article'>";

                    if ($isEdit) {
                        echo "<input hidden name='id' value='".$_GET['id']."'>";
                        echo "<input hidden name='image_old' value='".$article->image."'>";
                    }
                ?>
            </div>
        </div>
    </form>
</div>