<?php
    require_once $dir . "models/User.php";
    require_once $dir . 'core/DataBase.php';

    $controller = "/controllers/UserController.php?id=$id&url_back="
        .$urls['back']."&action=$action&action_back=list&module=user";
    $name = "запись";
    $table_name = "таблица не задана";

    $db = new DataBase();
    $db->connect();

    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $user = new User();
    $user->setDbConnection($db);

    if ($user->get('id', $id)) {
        $role_user = ($user->role == 1) ? "Администратор" : "Пользователь";
        $name = "'$user->first_name $user->last_name - $user->username - $user->date_registration - $role_user'";
        $table_name = $user->table_name;
    } else {
        echo $user->error;
        exit;
    }
?>

<div class="row row-padding">
    <div class="col-1"></div>
    <div class="col-11">
        <?php
            require_once $dir . "views/deleteElement.php";
        ?>
    </div>
</div>