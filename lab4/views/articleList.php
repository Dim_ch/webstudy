<?php 
    $count = count($articles);
?>

<div class="row row-padding">
    <div class="col-12 table-container mb-4">
        <table>
            <tr>
                <?php
                    if($isAdmin) {
                        echo '<th>';
                        echo '<a href="'.$urls['create'].'">Добавить</a>';
                        echo '</th>';
                    }
                    echo '<th>Заголовок</th>';
                    echo '<th>Текст</th>';
                    echo '<th>Изображение</th>';
                ?>
            </tr>
            <?php
                foreach($articles as $cur_article) {
                    echo '<tr>';
                        if($isAdmin) {
                            echo "<td>";
                                echo "<a href='".$urls['edit']."&id=".$cur_article['id']."'>Редактировать</a>";
                                echo "<a href='".$urls['delete']."&id=".$cur_article['id']."'>Удалить</a>";
                                echo "<a href='".$urls['view']."&id=".$cur_article['id']."'>Смотреть</a>";
                            echo "</td>";
                        }
                        echo "<td>" . $cur_article['head'] . "</td>";
                        echo "<td>" . mb_substr($cur_article['article'], 0, 200, 'utf-8') . "...</td>";
                        echo "<td>" . $cur_article['image'] . "</td>";
                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</div>