<?php
    require_once $dir . 'core/DataBase.php';
    require_once $dir . 'models/User.php';

    $valueButton = "Добавить";
    $db = new DataBase();
    $db->connect();
    
    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $username = isset($_REQUEST['username']) ? $_REQUEST['username'] : '';
    $first_name = isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : '';
    $last_name = isset($_REQUEST['last_name']) ? $_REQUEST['last_name'] : '';
    $email = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
    $role = 2;
    $date_registration;

    $user = new User();
    $user->setDbConnection($db);

    $isEdit = isset($_GET['id']);
    
    if($isEdit) {
        $valueButton = "Редактировать";
        
        if (!$user->get('id', $_GET['id'])) {
            echo($user->error);
            exit;
        }

        $username = $user->username;
        $first_name = $user->first_name;
        $last_name = $user->last_name;
        $email = $user->email;
        $role = $user->role;
        $date_registration = $user->date_registration;
    }
?>
<div class="row">
    <form
        class="container row-padding register_block mt-5 mb-5"
        method="POST"
        style="width: 80%"
        action="/controllers/UserController.php"
    >
        <div class="col-12">
            <p>
                <a class="register_block__a" href="<?= "$url_back?action=$action_back&id=$id&module=user" ?>">Назад</a>
            </p>
        </div>

        <h2 class="text-center"><?= $title ?></h2>

        <div class="divider_footer"></div>

        <div class="row mb-4 mt-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Имя: </p>
            </div>
            <div class="col-8">
                <input type="text" name="first_name" value="<?= isset($first_name) ? $first_name: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Фамилия: </p>
            </div>
            <div class="col-8">
                <input type="text" name="last_name" value="<?= isset($last_name) ? $last_name: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Логин: </p>
            </div>
            <div class="col-8">
                <input type="text" name="username" value="<?= isset($username) ? $username: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Почта: </p>
            </div>
            <div class="col-8">
                <input type="text" name="email" value="<?= isset($email) ? $email: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Пароль: </p>
            </div>
            <div class="col-8">
                <input type="password" name="password">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Роль: </p>
            </div>
            <div class="col-8">
                <select name="role">
                    <?php
                        echo "<option value='1' ".(($role == 1) ? 'selected' : '').">Администратор</option>";
                        echo "<option value='2' ".(($role == 2) ? 'selected' : '').">Пользователь</option>";
                    ?>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <?php
                        echo "<input class='btn-register' type='submit' name='btn_ok' value='".$valueButton."'>";
                        echo "<input hidden name='action' value='".$action."'>";
                        echo "<input hidden name='action_back' value='$action_back'>";
                        echo "<input hidden name='url_back' value='".$url_back."'>";
                        echo "<input hidden name='module' value='user'>";

                        if ($isEdit) {
                            echo "<input hidden name='id' value='".$_GET['id']."'>";
                            echo "<input hidden name='date_registration' value='$date_registration'>";
                        }
                ?> 
            </div>
        </div>
    </form>
</div>