<?php
    require_once $dir . 'core/DataBase.php';
    require_once $dir . 'models/Plane.php';
    
    $valueButton = "Добавить";
    
    $db = new DataBase();
    $db->connect();
    
    if (!$db->isReady()) {
        echo "Ошибка на сервере";
        exit;
    }

    $plane = new Plane();
    $plane->setDbConnection($db);
    
    $isEdit = isset($_GET['id']);
    if($isEdit) {
        $valueButton = "Редактировать";
        
        if (!$plane->get('id', $_GET['id'])) {
            echo($plane->error);
            exit;
        }
    }
?>

<div class="row register">
    <form
    class="container row-padding register_block mt-5 mb-5"
    action="/controllers/PlaneController.php"
    method="POST"
    style="width: 80%"
    enctype="multipart/form-data"
    >

        <div class="col-12">
            <p>
                <a class="register_block__a" href="<?= "$url_back?action=$action_back&id=$id&module=plane" ?>">Назад</a>
            </p>
        </div>
    
        <h2 class="text-center"><?= $title ?></h2>

        <div class="divider_footer"></div>

        <div class="row mb-4 mt-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Модель: </p>
            </div>
            <div class="col-8">
                <input type="text" name="name" value="<?= $isEdit ? $plane->name: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Дальность полета (км): </p>
            </div>
            <div class="col-8">
                <input type="text" name="range" value="<?= $isEdit ? $plane->range: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Крейсерская скорость (км/ч): </p>
            </div>
            <div class="col-8">
                <input type="text" name="speed" value="<?= $isEdit ? $plane->speed: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Максимальное число пассажиров: </p>
            </div>
            <div class="col-8">
                <input type="text" name="passengers" value="<?= $isEdit ? $plane->passengers: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Длина: </p>
            </div>
            <div class="col-8">
                <input type="text" name="length" value="<?= $isEdit ? $plane->length: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Ширина: </p>
            </div>
            <div class="col-8">
                <input type="text" name="width" value="<?= $isEdit ? $plane->width: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Высота: </p>
            </div>
            <div class="col-8">
                <input type="text" name="height" value="<?= $isEdit ? $plane->height: ''; ?>">
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Описание: </p>
            </div>
            <div class="col-8">
                <textarea name="description" rows="7"><?= $isEdit ? $plane->description: ''; ?></textarea>
            </div>
        </div>

        <?php
            if (!empty($plane->image)) {
                echo "<div class='row mb-4'>";
                    echo "<div class='col-4 text-right'>";
                        echo "<p class='mb-0 mt-0'>Текущее изображение: </p>";
                    echo "</div>";
                    echo "<div class='col-8'>";
                        echo "<div class='catalog_img'>";
                        echo "<img class='article_main_img' src='/$plane->image' alt='$plane->name'>";
                        echo "</div>";
                    echo "</div>";
                echo "</div>";
            }
        ?>

        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Главное изображение: </p>
            </div>
            <div class="col-8">
                <input type="file" name="image">
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-center">
                <?php
                    echo "<input class='btn-register' type='submit' name='btn_ok' value='".$valueButton."'>";
                    echo "<input hidden name='action' value='".$action."'>";
                    echo "<input hidden name='action_back' value='$action_back'>";
                    echo "<input hidden name='url_back' value='".$url_back."'>";
                    echo "<input hidden name='user_id' value='".$_SESSION['user_id']."'>";
                    echo "<input hidden name='module' value='plane'>";

                    if ($isEdit) {
                        echo "<input hidden name='id' value='".$_GET['id']."'>";
                        echo "<input hidden name='image_old' value='".$plane->image."'>";
                        echo "<input hidden name='user_id' value='".$plane->user_id."'>";
                    }
                ?>
            </div>
        </div>
    </form>
</div>