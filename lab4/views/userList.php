<?php 
    $count = count($users);
?>

<div class="row row-padding">
    <div class="col-12 table-container mb-4">
        <table>
            <tr>
                <?php
                    if($isAdmin) {
                        echo '<th>';
                        echo '<a href="'.$urls['create'].'">Добавить</a>';
                        echo '</th>';
                    }
                    echo '<th>Имя</th>';
                    echo '<th>Фамилия</th>';
                    echo '<th>Логин</th>';
                    echo '<th>Пароль</th>';
                    echo '<th>email</th>';
                    echo '<th>Дата регистрации</th>';
                    echo '<th>Роль</th>';

                ?>
            </tr>
            <?php
                foreach($users as $cur_user) {
                    $role = ($cur_user['role'] == 1) ? 'Админ' : 'Пользователь';
                    echo '<tr>';
                        if($isAdmin) {
                            echo "<td>";
                                echo "<a href='".$urls['edit']."&id=".$cur_user['id']."'>Редактировать</a>";
                                if ($user_id != $cur_user['id']) {
                                    echo "<a href='".$urls['delete']."&id=".$cur_user['id']."'>Удалить</a>";
                                }
                            echo "</td>";
                        }
                        echo "<td>" . $cur_user['first_name'] . "</td>";
                        echo "<td>" . $cur_user['last_name'] . "</td>";
                        echo "<td>" . $cur_user['username'] . "</td>";
                        echo "<td>" . $cur_user['password'] . "</td>";
                        echo "<td>" . $cur_user['email'] . "</td>";
                        echo "<td>" . $cur_user['date_registration'] . "</td>";
                        echo "<td>$role</td>";
                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</div>