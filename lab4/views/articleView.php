<?php
    echo "<div class='row slider'>";
        echo "<img class='article_main_img' src='/".$article->image."' alt='".$article->head."'>";
    echo "</div>";

    echo "<div class='divider'></div>";

    echo "<div class='row row-padding justify-content-center'>";
        echo "<div class='col-12 col-lg-9 article'>";
            echo "<h2 class='text-center news_title'>".$article->head."</h2>";
            echo "<a href='".$urls['back']."?action=$action_back&id=$id&module=$module'>Вернуться назад</a>";
            if ($isAdmin && isset($_REQUEST['module'])) {
                echo "<a href='".$urls['back']."?action=edit&url_back=$url_back&action_back=view&id=$id&module=$module'>Редактировать</a>";
                echo "<a href='".$urls['back']."?action=delete&url_back=$url_back&action_back=view&id=$id&module=$module'>Удалить</a>";
            }
            echo "<p>".$article->article."</p>";
        echo "</div>";
    echo "</div>";
    
    echo "<div class='divider'></div>";
?>