let sliderItems = document.getElementsByClassName('slider_item');
let slickItems = document.querySelectorAll('.slick > li');
let slickArray = Array.from(slickItems);

let curI = 0;
let lenItems = sliderItems.length;

let sliderTimer;
const timer = 10000;

sliderItems[curI].className = 'slider_item active';
slickItems[curI].className = 'active';

function toglleSlide(prev, next) {
    sliderItems[prev].className = 'slider_item';
    sliderItems[next].className = 'slider_item active';
    slickItems[prev].className = '';
    slickItems[next].className = 'active';
}

function slideNext(event) {
    if (!!event && (event.type === 'click'))
        clearInterval(sliderTimer);

    let prev = curI;
    ++curI;

    if (curI > lenItems - 1) {
        curI = 0;
    }

    toglleSlide(prev, curI);
    if (!!event)
        sliderTimer = setInterval(slideNext, timer);
}

function slidePrev(event) {
    if (!!event && (event.type === 'click'))
        clearInterval(sliderTimer);

    let prev = curI;
    --curI;

    if (curI < 0) {
        curI = lenItems - 1;
    }

    toglleSlide(prev, curI);
    if (!!event)
        sliderTimer = setInterval(slideNext, timer);
}

function slideSlickClick(event) {
    clearInterval(sliderTimer);
    let next = slickArray.indexOf(event.target);
    if (next !== curI) toglleSlide(curI, next);
    curI = next;
    sliderTimer = setInterval(slideNext, timer);
}

document.querySelector('.slider > .next').addEventListener('click', slideNext);
document.querySelector('.slider > .prev').addEventListener('click', slidePrev);

for (slick of slickItems)
    slick.addEventListener('click', slideSlickClick);

sliderTimer = setInterval(slideNext, timer);