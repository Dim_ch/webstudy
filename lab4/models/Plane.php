<?php 
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    require_once $dir . 'models/Model.php';

    class Plane implements Model {
        public $id;
        public $name;
        public $range;
        public $speed;
        public $passengers;
        public $length;
        public $width;
        public $height;
        public $description;
        public $gallery;
        public $user_id;
        public $image;
        
        public $table_name = 'plane';
        public $error = "";
        public $db_connection;

        public function setDbConnection(DataBase $db) {
            $this->db_connection = $db->getConnection();
        }

        public function insert() {
            $result = false;

            $sql_command = "
                INSERT INTO $this->table_name(`name`, `range`, `speed`, `passengers`, `length`,
                `width`, `height`, `description`, `gallery`, `user_id`, `image`) " . 
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                if ($stmt = $this->db_connection->prepare($sql_command)) {
                    
                    if ($stmt->bind_param("siiidddsiis",
                        $this->name,
                        $this->range,
                        $this->speed,
                        $this->passengers,
                        $this->length,
                        $this->width,
                        $this->height,
                        $this->description,
                        $this->gallery,
                        $this->user_id,
                        $this->image
                        )) {
                            if ($stmt->execute()) {
                                $result =  true;
                            } else {
                                $this->error = $stmt->error;
                            }
                    } else {
                        $this->error = $stmt->error;
                    }
                } else {
                    $this->error = $this->db_connection->error;
            }

            return $result;
        }
        
        public function update() {
            $result = false;

            $sql_command = "
                UPDATE  `$this->table_name` SET `name` = (?), `range` = (?), `speed` = (?),
                `passengers` = (?), `length` = (?), `width` = (?), `height` = (?),
                `description` = (?), `gallery` = (?), `user_id` = (?), `image` = (?)
                WHERE id = $this->id";

                if ($stmt = $this->db_connection->prepare($sql_command)) {
                    
                if ($stmt->bind_param("siiidddsiis",
                    $this->name,
                    $this->range,
                    $this->speed,
                    $this->passengers,
                    $this->length,
                    $this->width,
                    $this->height,
                    $this->description,
                    $this->gallery,
                    $this->user_id,
                    $this->image
                    )) {
                        if ($stmt->execute()) {
                            $result =  true;
                        } else {
                            $this->error = $stmt->error;
                        }
                    } else {
                        $this->error = $stmt->error;
                    }
                } else {
                    $this->error = $this->db_connection->error;
            }

            return $result;
        }
    
        public function delete() {
            $sql_command = "DELETE FROM $this->table_name WHERE id = $this->id;";

            if ($this->db_connection->query($sql_command)) {
                return true;
            } else {
                $this->error = $this->db_connection->error;
                return false;
            }
        }
    
        public function all() {
            $sql_command = "SELECT * FROM $this->table_name;";

            $result = $this->db_connection->query($sql_command);

            return $result->fetch_all(MYSQLI_ASSOC);
        }
    
        public function get(string $field, $value) {
            $result = false;

            $value = (strcmp(gettype($value), 'string') == 0) ? "'$value'" : "$value";
            
            $sql_command = "SELECT `id`, `name`, `range`, `speed`, `passengers`, `length`,
            `width`, `height`, `description`, `gallery`, `user_id`, `image` 
            FROM `$this->table_name`
            WHERE $field = $value;";

            if ($stmt = $this->db_connection->prepare($sql_command)) {

                if ($stmt->execute()) {

                    $stmt->store_result();
                    $count = $stmt->num_rows;

                    if ($count == 0) {
                        $this->error = "Не удалось найти самолет с $field: $value";
                    } else {
                        if ($stmt->bind_result(
                            $this->id,
                            $this->name,
                            $this->range,
                            $this->speed,
                            $this->passengers,
                            $this->length,
                            $this->width,
                            $this->height,
                            $this->description,
                            $this->gallery,
                            $this->user_id,
                            $this->image
                        )) {
                            $result = $stmt->fetch();
                        } else {
                            $this->error = $stmt->error;
                        }
                    }
                } else {
                    $this->error = $stmt->error;
                }
            } else {
                $this->error = $this->db_connection->error;
            }

            return $result;
        }
    }
?>