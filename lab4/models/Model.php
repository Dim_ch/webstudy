<?php
    interface Model {

        function insert();

        function update();

        function delete();

        function all();

        function get(string $field, $value);

        function setDbConnection(DataBase $db);
    }
?>