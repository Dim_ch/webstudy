<?php 
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    require_once $dir . 'models/Model.php';

    class User implements Model {
        public $id;
        public $first_name;
        public $last_name;
        public $username;
        public $password;
        public $email;
        public $date_registration;
        public $role;
        
        public $table_name = 'user';
        public $db_connection;
        public $error = "";

        public function setDbConnection(DataBase $db) {
            $this->db_connection = $db->getConnection();
        }
        
        public function insert() {
            $result = false;

            $sql_command = "
                INSERT INTO " . $this->table_name . "(first_name, last_name, " . 
                "username, password, email, date_registration, role)" . 
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
            if ($stmt = $this->db_connection->prepare($sql_command)) {

                if ($stmt->bind_param("ssssssi",
                        $this->first_name,
                        $this->last_name,
                        $this->username,
                        $this->password,
                        $this->email,
                        $this->date_registration,
                        $this->role
                    )) {
                        if ($stmt->execute()) {
                            $result =  true;
                        } else {
                            $this->error = $stmt->error;
                        }
                } else {
                    $this->error = $stmt->error;
                }
            } else {
                $this->error = $this->db_connection->error;
            }

            return $result;
        }
        
        public function update() {
            $result = false;

            $sql_command = "
                UPDATE  `$this->table_name` SET `first_name` = (?), `last_name` = (?), `username` = (?),
                `password` = (?), `email` = (?), `date_registration` = (?), `role` = (?)
                WHERE id = $this->id";

                if ($stmt = $this->db_connection->prepare($sql_command)) {
                    
                if ($stmt->bind_param("ssssssi",
                    $this->first_name,
                    $this->last_name,
                    $this->username,
                    $this->password,
                    $this->email,
                    $this->date_registration,
                    $this->role
                    )) {
                        if ($stmt->execute()) {
                            $result =  true;
                        } else {
                            $this->error = $stmt->error;
                        }
                    } else {
                        $this->error = $stmt->error;
                    }
                } else {
                    $this->error = $this->db_connection->error;
            }

            return $result;
        }
    
        public function delete() {
            $sql_command = "DELETE FROM $this->table_name WHERE id = $this->id;";

            if ($this->db_connection->query($sql_command)) {
                return true;
            } else {
                $this->error = $this->db_connection->error;
                return false;
            }
        }
    
        public function all() {
            $sql_command = "SELECT * FROM $this->table_name;";

            $result = $this->db_connection->query($sql_command);

            return $result->fetch_all(MYSQLI_ASSOC);
        }

        public function get(string $field, $value) {
            $result = false;

            $value = (strcmp(gettype($value), 'string') == 0) ? "'$value'" : "$value";
 
            $sql_command = "SELECT U.id, U.first_name, U.last_name, U.username, 
            U.password, U.email, U.date_registration, U.role FROM $this->table_name
            AS U WHERE U.$field = $value;";

            if ($stmt = $this->db_connection->prepare($sql_command)) {

                if ($stmt->execute()) {

                    $stmt->store_result();
                    $count = $stmt->num_rows;

                    if ($count == 0) {
                        $this->error = "Не удалось найти пользоваталея с $field: $value";
                    } else {
                        if ($stmt->bind_result(
                            $this->id,
                            $this->first_name,
                            $this->last_name,
                            $this->username,
                            $this->password,
                            $this->email,
                            $this->date_registration,
                            $this->role
                        )) {
                            $result = $stmt->fetch();
                        } else {
                            $this->error = $stmt->error;
                        }
                    }
                } else {
                    $this->error = $stmt->error;
                }
            } else {
                $this->error = $this->db_connection->error;
            }

            return $result;
        }
    }
?>