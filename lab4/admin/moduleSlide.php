<?php
    require_once $dir . "models/Slide.php";
    require_once $dir . 'core/DataBase.php';
    
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    } else $action = 'list';

    switch($action) {
        case 'list':
            $title = 'Просмотр слайдов';
            $view = 'views/slideList.php';

            $db = new DataBase();
            $db->connect();
        
            if (!$db->isReady()) {
                echo "Ошибка на сервере";
                exit;
            }
        
            $slide = new Slide();
            $slide->setDbConnection($db);
            $slides = $slide->all();
        break;

        case 'create':
            $title = 'Добавление слайда';
            $view = 'views/slideCreate.php';
            $url_back = $urls['back'];
            $action_back = 'list';
            break;

        case 'edit':
            $title = 'Редактирование слайда';
            
            if (isset($_GET['url_back'])) {
                $url_back = $_GET['url_back'];
                if (isset($_GET['action_back'])) {
                    $action_back = $_GET['action_back'];
                }
            } else {
                $url_back = $urls['back'];
                $action_back = 'list';
            }

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/slideCreate.php";
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
            break;

        case 'delete':
            $title = 'Удаление слайда';
            
            if (isset($_GET['url_back'])) {
                $url_back = $_GET['url_back'];
                if (isset($_GET['action_back'])) {
                    $action_back = $_GET['action_back'];
                }
            } else {
                $url_back = $urls['back'];
                $action_back = 'list';
            }

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/slideDelete.php";
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
            break;

        case 'view':
            $title = 'Просмотр слайда';
            $isInfo = true;
            
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/slideView.php";

                if (isset($_GET['url_back'])) {
                    $url_back = $_GET['url_back'];
                    if (isset($_GET['action_back'])) {
                        $action_back = $_GET['action_back'];
                    }
                } else {
                    $url_back = $urls['back'];
                    $action_back = 'list';
                }

                $db = new DataBase();
                $db->connect();

                if (!$db->isReady()) {
                    echo "Ошибка на сервере";
                    exit;
                }

                $slide = new Slide();
                $slide->setDbConnection($db);

                if ($slide->get('id', $id)) {
                    $slides[] = (array) $slide;
                } else {
                    echo $slide->error;
                    exit;
                }
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
        break;
    }
?>