<?php
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    $view;
    $module;
    $urls;
    $action;
    $action_back;

    require_once $dir . "controllers/watchCookie.php";

    if (!$isAdmin) {
        $_SESSION['message'] .= "Войдите как администратор";
        header("Location: /content/login.php");
        exit;
    }

    if (isset($_GET['module'])) {
        $module = $_GET['module'];
    } else $module = 'panel';

    $urls = array(
        'list' => "/admin/index.php?module=$module&action=list",
        'create' => "/admin/index.php?module=$module&action=create",
        'edit' => "/admin/index.php?module=$module&action=edit",
        'delete' => "/admin/index.php?module=$module&action=delete",
        'view' => "/admin/index.php?module=$module&action=view",
        'back' => "/admin/index.php"
    );

    switch($module) {
        case 'article':
            $module_include = "admin/moduleArticle.php";
            break;
        case 'panel':
            $module_include = "admin/modulePanel.php";
            break;
        case 'plane':
            $module_include = "admin/modulePlane.php";
            break;
        case 'slide':
            $module_include = "admin/moduleSlide.php";
            break;
        case 'user':
            $module_include = "admin/moduleUser.php";
            break;
    }

    include_once $dir.$module_include;
    require_once $dir."components/AdminHeader.php";
?>
<body>
<?php
    require_once $dir."components/AdminNavigation.php";
?>

<main class="container">
    <?php
        require_once $dir."components/showMessage.php";

        echo "<h3>$title</h3>";
        
        if (strcmp($module, 'panel') != 0 && strcmp($action, 'list') == 0) {
            echo "<div class='row'><div class='col-12 mt-3' style='padding-left: 30px;'>";
            echo "<a href='".$urls['back']."?module=panel'>Вернуться на главную панель</a>";
            echo "</div></div>";
        }

        require_once $dir.$view;
    ?>
</main>

</body>
</html>