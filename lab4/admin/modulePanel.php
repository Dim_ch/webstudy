<?php
    $view = 'views/adminPanel.php';
    $title = 'Панель администратора';
    $urls = array(
        'article' => '/admin/index.php?module=article',
        'plane' => '/admin/index.php?module=plane',
        'slide' => '/admin/index.php?module=slide',
        'user' => '/admin/index.php?module=user'
    );
?>