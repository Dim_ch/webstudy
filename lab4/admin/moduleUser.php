<?php
    require_once $dir . "models/User.php";
    require_once $dir . 'core/DataBase.php';
    
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    } else $action = 'list';

    switch($action) {
        case 'list':
            $title = 'Просмотр пользователей';
            $view = 'views/userList.php';

            $db = new DataBase();
            $db->connect();
        
            if (!$db->isReady()) {
                echo "Ошибка на сервере";
                exit;
            }
        
            $user = new User();
            $user->setDbConnection($db);
            $users = $user->all();
        break;

        case 'create':
            $title = 'Добавление пользователя';
            $view = 'views/userCreate.php';
            $url_back = $urls['back'];
            $action_back = 'list';
            break;

        case 'edit':
            $title = 'Редактирование пользователя';
            
            if (isset($_GET['url_back'])) {
                $url_back = $_GET['url_back'];
                if (isset($_GET['action_back'])) {
                    $action_back = $_GET['action_back'];
                }
            } else {
                $url_back = $urls['back'];
                $action_back = 'list';
            }

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/userCreate.php";
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
            break;

        case 'delete':
            $title = 'Удаление пользователя';
            
            if (isset($_GET['url_back'])) {
                $url_back = $_GET['url_back'];
                if (isset($_GET['action_back'])) {
                    $action_back = $_GET['action_back'];
                }
            } else {
                $url_back = $urls['back'];
                $action_back = 'list';
            }

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/userDelete.php";
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
        break;
    }
?>