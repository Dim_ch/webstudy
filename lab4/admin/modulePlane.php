<?php
    require_once $dir . "models/Plane.php";
    require_once $dir . 'core/DataBase.php';
    
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    } else $action = 'list';

    switch($action) {
        case 'list':
            $title = 'Просмотр самолётов';
            $view = 'views/planeList.php';
            $isInfo = false;

            $db = new DataBase();
            $db->connect();
        
            if (!$db->isReady()) {
                echo "Ошибка на сервере";
                exit;
            }
        
            $plane = new Plane();
            $plane->setDbConnection($db);
            $planes = $plane->all();
        break;

        case 'create':
            $title = 'Добавление самолёта';
            $view = 'views/planeCreate.php';
            $url_back = $urls['back'];
            $action_back = 'list';
            break;

        case 'edit':
            $title = 'Редактирование самолёта';
            
            if (isset($_GET['url_back'])) {
                $url_back = $_GET['url_back'];
                if (isset($_GET['action_back'])) {
                    $action_back = $_GET['action_back'];
                }
            } else {
                $url_back = $urls['back'];
                $action_back = 'list';
            }

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/planeCreate.php";
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
            break;

        case 'delete':
            $title = 'Удаление самолёта';
            
            if (isset($_GET['url_back'])) {
                $url_back = $_GET['url_back'];
                if (isset($_GET['action_back'])) {
                    $action_back = $_GET['action_back'];
                }
            } else {
                $url_back = $urls['back'];
                $action_back = 'list';
            }

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/planeDelete.php";
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
            break;

        case 'view':
            $title = 'Просмотр самолёта';
            $isInfo = true;
            $isEditable = true;
            
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/planeView.php";

                $url_back = $urls['back'];

                $db = new DataBase();
                $db->connect();

                if (!$db->isReady()) {
                    echo "Ошибка на сервере";
                    exit;
                }

                $plane = new Plane();
                $plane->setDbConnection($db);

                if ($plane->get('id', $id)) {
                    $planes[] = (array) $plane;
                } else {
                    echo $plane->error;
                    exit;
                }
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
        break;
    }
?>