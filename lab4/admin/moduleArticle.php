<?php
    require_once $dir . "models/Article.php";
    require_once $dir . 'core/DataBase.php';
    
    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    } else $action = 'list';

    switch($action) {
        case 'list':
            $title = 'Просмотр статей';
            $view = 'views/articleList.php';

            $db = new DataBase();
            $db->connect();
        
            if (!$db->isReady()) {
                echo "Ошибка на сервере";
                exit;
            }
        
            $article = new Article();
            $article->setDbConnection($db);
            $articles = $article->all();
        break;

        case 'create':
            $title = 'Добавление статьи';
            $view = 'views/articleCreate.php';
            $url_back = $urls['back'];
            $action_back = 'list';
            break;

        case 'edit':
            $title = 'Редактирование статьи';
            
            if (isset($_GET['url_back'])) {
                $url_back = $_GET['url_back'];
                if (isset($_GET['action_back'])) {
                    $action_back = $_GET['action_back'];
                }
            } else {
                $url_back = $urls['back'];
                $action_back = 'list';
            }

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/articleCreate.php";
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
            break;

        case 'delete':
            $title = 'Удаление статьи';
            
            if (isset($_GET['url_back'])) {
                $url_back = $_GET['url_back'];
                if (isset($_GET['action_back'])) {
                    $action_back = $_GET['action_back'];
                }
            } else {
                $url_back = $urls['back'];
                $action_back = 'list';
            }

            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/articleDelete.php";
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
            break;

        case 'view':
            $title = 'Просмотр статьи';
            $isInfo = true;
            
            if (isset($_GET['id'])) {
                $id = $_GET['id'];
                $view = "views/articleView.php";

                if (isset($_GET['url_back'])) {
                    $url_back = $_GET['url_back'];
                    if (isset($_GET['action_back'])) {
                        $action_back = $_GET['action_back'];
                    }
                } else {
                    $url_back = $urls['back'];
                    $action_back = 'list';
                }

                $db = new DataBase();
                $db->connect();

                if (!$db->isReady()) {
                    echo "Ошибка на сервере";
                    exit;
                }

                $article = new Article();
                $article->setDbConnection($db);

                if ($article->get('id', $id)) {
                    $articles[] = (array) $article;
                } else {
                    echo $article->error;
                    exit;
                }
            } else {
                echo 'Что-то пошло не так';
                exit;
            }
        break;
    }
?>