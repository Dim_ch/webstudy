<?php 
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';

    require $dir . 'controllers/dbController.php';

    if (isset($_GET['id'])) {
        $strDbXml = $dir . 'db.xml';
        $dbController = new dbController($strDbXml);
        $dbController->deletePlane($_GET['id']);
        $dbController->saveDb();
    }

    header('Location: /content/db.php');
?>