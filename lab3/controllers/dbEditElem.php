<?php 
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    require $dir . 'controllers/dbController.php';

    $plane;
    $plane['id'] = $_POST['id'];

    if (isset($_POST['name'])) {
        $plane['name'] = $_POST['name'];
    }
    else $plane['name'] = "";

    if (isset($_POST['range'])) {
        $plane['range'] = $_POST['range'];
    }
    else $plane['range'] = "";

    if (isset($_POST['speed'])) {
        $plane['speed'] = $_POST['speed'];
    }
    else $plane['speed'] = "";

    if (isset($_POST['passengers'])) {
        $plane['passengers'] = $_POST['passengers'];
    }
    else $plane['passengers'] = "";

    if (isset($_POST['length'])) {
        $plane['length'] = $_POST['length'];
    }
    else $plane['length'] = "";

    if (isset($_POST['width'])) {
        $plane['width'] = $_POST['width'];
    }
    else $plane['width'] = "";

    if (isset($_POST['height'])) {
        $plane['height'] = $_POST['height'];
    }
    else $plane['height'] = "";

    $strDbXml = $dir . 'db.xml';
    $dbController = new dbController($strDbXml);
    $dbController->editPlane($plane);
    $dbController->saveDb();
    
    header('Location: /content/db.php');
?>