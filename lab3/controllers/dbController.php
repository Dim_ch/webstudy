<?php 
    class DbController {
        private $dbXml;
        private $filePath;

        function __construct($strXml) {
            $this->dbXml = simplexml_load_file($strXml);
            $this->filePath = $strXml;
        }

        public function getPlanes() {
            return $this->dbXml->children();
        }
        
        public function getPlane($id) {
            foreach($this->dbXml as $plane) {
                if ($plane['id'] == $id) {
                    return $plane;
                }
            }
        }

        public function deletePlane($id) {
            $newXml = new SimpleXMLElement('<planes></planes>');
            foreach($this->dbXml as $plane) {
                if ($plane['id'] != $id) {
                    $planeClone = $newXml->addChild('plane');
                    
                    $planeClone['id'] = $plane['id'];
                    $planeClone->addChild('name', $plane->name);
                    $planeClone->addChild('range', $plane->range);
                    $planeClone->addChild('speed', $plane->speed);
                    $planeClone->addChild('passengers', $plane->passengers);
                    $planeClone->addChild('length', $plane->length);
                    $planeClone->addChild('width', $plane->width);
                    $planeClone->addChild('height', $plane->height);
                }
            }
            $this->dbXml = $newXml;
        }
        
        public function createPlane($plane) {
            $id = $this->dbXml->plane[$this->dbXml->count() - 1]['id'] + 1;

            $planeNew = $this->dbXml->addChild('plane');

            $planeNew['id'] = $id;
            $planeNew->addChild('name', $plane['name']);
            $planeNew->addChild('range', $plane['range']);
            $planeNew->addChild('speed', $plane['speed']);
            $planeNew->addChild('passengers', $plane['passengers']);
            $planeNew->addChild('length', $plane['length']);
            $planeNew->addChild('width', $plane['width']);
            $planeNew->addChild('height', $plane['height']);
        }
        
        public function editPlane($planeEdit) {
            foreach($this->dbXml as $plane) {
                if ($plane['id'] == $planeEdit['id']) {
                    $plane->name = $planeEdit['name'];
                    $plane->range = $planeEdit['range'];
                    $plane->speed =$planeEdit['speed'];
                    $plane->passengers = $planeEdit['passengers'];
                    $plane->length = $planeEdit['length'];
                    $plane->width = $planeEdit['width'];
                    $plane->height = $planeEdit['height'];
                    break;
                }
            }
        }
        
        public function saveDb() {
            $this->dbXml->asXml($this->filePath);
        }
    }
?>