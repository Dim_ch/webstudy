<?php

    $title = 'Главная';

    require 'components/header.php';

    require 'content/home.php';

    require 'components/footer.php';
?>
<script type="text/javascript">
    let startTime = new Date();
    let timerId = null;

    document.querySelector('.news_title')
    .addEventListener('mousedown', function(event) {
        if (event.target.nodeName == 'SPAN') return;
        clearTimeout(timerId);

        let end = new Date();
        let elem = event.target.querySelector('span.tooltip');
        let timeState = (end - startTime) / 1000;
        let n = +(Math.random() * Math.floor(timeState)).toFixed(2);
        n = (n === 0) ? 1 : n;
        let m = Math.log2(n);
        let text = `Вы находитесь на сайте уже ${Math.floor(timeState)} секунд<br>
                    log2(${n}) = ${m}.<br>Откуда здесь log2(${n})?`

        elem.style.display = 'block';
        elem.innerHTML = text;

        timerId = setTimeout(function() {
            elem.style.display = 'none';
        }, 5000);
    });
</script>
</body>
</html>