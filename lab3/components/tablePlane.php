<div class="col-12 table-container mb-4">
    <table>
        <tr>
            <th>Модель</th>
            <th>Дальность полета<br>(км)</th>
            <th>Крейсерская скорость<br>(км/ч)</th>
            <th>Максимальное число пассажиров</th>
            <th>Длина</th>
            <th>Ширина</th>
            <th>Высота</th>
            <?php if(!$isInfo): ?>
                <th>
                    <a href="/content/planeCreate.php">Добавить</a>
                </th>
            <?php endif; ?>
        </tr>
        <?php foreach($planes as $plane) { ?>
        <tr>
            <td><?= $plane->name ?></td>
            <td><?= $plane->range ?></td>
            <td><?= $plane->speed ?></td>
            <td><?= $plane->passengers ?></td>
            <td><?= $plane->length ?></td>
            <td><?= $plane->width ?></td>
            <td><?= $plane->height ?></td>
            <?php if(!$isInfo): ?>
                <td>
                    <a class="btn_edit" href="/content/planeCreate.php?id=<?= $plane['id'] ?>">Редактировать</a>
                    <a class="btn_delete" href="/controllers/dbDeleteElem.php?id=<?= $plane['id'] ?>">Удалить</a>
                </td>
            <?php endif; ?>
        </tr>
        <?php } ?>
    </table>
</div>