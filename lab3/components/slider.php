<?php 
    $scripts[] = "/js/slider.js";
?>
<div class="row slider">
    <ul class="slick">
        <li></li>
        <li></li>
        <li></li>
    </ul>

    <div class="slider_item">
        <h1>LIFE IS EASY WITH A SKYLINE</h1>
        <img src="image/plane.jpg" alt="image/plane.jpg">
        <p>Real world performance doesn’t have to be hard.</p>
    </div>
    <div class="slider_item">
        <h1>Enter a World Where Accomplishment Is Felt in Every Stitch</h1>
        <img src="image/news1.jpg" alt="image/news1.jpg">
        <p>INTRODUCING THE CITATION CJ4 GEN2 JET</p>
    </div>
    <div class="slider_item">
        <h1>TURBO STATIONAIR HD SIMPLY GETS IT DONE</h1>
        <img src="image/news2.jpg" alt="image/news2.jpg">
        <p>The turbocharged capability of the Cessna Turbo Stationair HD is designed to fit your mission needs.</p>
    </div>

    <a href="#" class="next"><i class="right"></i></a>
    <a href="#" class="prev"><i class="left"></i></a>
</div>