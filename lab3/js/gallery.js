let galleryViewBlock = document.querySelector('.gallery_view_block');
let galleryLit = document.querySelectorAll('.gallery_list_img');

function galleryListClick(event) {
    if (event.target.parentElement.className.indexOf('active') != -1) return;

    document.querySelector('.gallery_list_img.active').className = 'gallery_list_img';
    event.target.parentElement.className = 'gallery_list_img active';

    let oldImg = document.querySelector('.gallery_view_block > .active_img');
    let newImg = document.createElement('img');

    oldImg.className = 'article_main_img delete_img';

    newImg.className = 'article_main_img active_img';
    newImg.src = event.target.src;
    newImg.alt = event.target.alt;

    galleryViewBlock.append(newImg);
    setTimeout(function() {
        oldImg.remove();
    }, 1000);
}

for (item of galleryLit)
    item.addEventListener('click', galleryListClick);
