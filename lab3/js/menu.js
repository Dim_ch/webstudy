let menu = document.querySelector('.navigation .list-menu-container');
let activeSubMenu = null;
let activeLi = null;
let start = '-100vw';
let midle = '-300px'
let end = '0px';

// Раскрывает меню
function showMenu() {
    let styles = menu.className.split(' ')
        .filter(x => x != 'show_menu' && x != 'hide_menu');

    styles.push('show_menu');
    
    menu.style.left = midle;
    menu.className = styles.join(' ');
    menu.style.left = end;
}

// Скрывает меню
function hideMenu() {
    let styles = menu.className.split(' ')
            .filter(x => x != 'show_menu' && x != 'hide_menu');
        styles.push('hide_menu');

        menu.className = styles.join(' ');
        menu.style.left = start;
}

function showSubMenu(event) {
    let subMenu = event.target.querySelector('ul');
    if (!subMenu) return;
    subMenu.style.display = 'block';
    activeSubMenu = subMenu;
}

function hideSubMenu(event) {
    if (!activeSubMenu) return;
    activeSubMenu.style.display = 'none';
    activeSubMenu = null;
}

// Раскрывает меню
document.querySelector('.navigation .btn-list').addEventListener('click', showMenu);
// Скрывает меню
menu.querySelector('.list-menu-empty').addEventListener('click', hideMenu);

for (item of menu.querySelectorAll('.list-menu-item-container > li')) {
    item.addEventListener('mouseenter', showSubMenu);
    item.addEventListener('mouseleave', hideSubMenu);
}