<?php
    $title = 'Каталог';
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    $scripts = array(
        '/js/filter.js'
    );


    require $dir . 'components/header.php';
?>

<main class="container">
    <div class="row">
        <div class="col-12 pt-4">
            <form class="form_search">
                <input type="search" name="search_plane" placeholder="Введите название"> 
                <button type="submit">Найти</button>
                </form>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row justify-content-center">
        <!-- Каталог -->
        <div class="col-12 col-md-9 col-xl-10 order-sm-2">
            <div class="row catalog">
                <div class="col-6 col-md-4 col-lg-3 mb-4">
                    <div class="catalog_img">
                        <img class="article_main_img" src="../image/cesna150.jpg" alt="img">
                    </div>
                    <div class="news_content p-2">
                        <h3 class="mb-0 mt-0">Cessna 150</h3>
                        <a href="planeInformation.php">Перейти</a>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-3 mb-4">
                    <div class="catalog_img">
                        <img class="article_main_img" src="../image/cessna_citation_cj3.jpg" alt="img">
                    </div>
                    <div class="news_content p-2">
                        <h3 class="mb-0 mt-1">Cessna 150</h3>
                        <a href="planeInformation.php">Перейти</a>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-3 mb-4">
                    <div class="catalog_img">
                        <img class="article_main_img" src="../image/cesna150.jpg" alt="img">
                    </div>
                    <div class="news_content p-2">
                        <h3 class="mb-0 mt-1">Cessna 150</h3>
                        <a href="planeInformation.php">Перейти</a>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-3 mb-4">
                    <div class="catalog_img">
                        <img class="article_main_img" src="../image/cessna_citation_cj3.jpg" alt="img">
                    </div>
                    <div class="news_content p-2">
                        <h3 class="mb-0 mt-1">Cessna 150</h3>
                        <a href="planeInformation.php">Перейти</a>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-3 mb-4">
                    <div class="catalog_img">
                        <img class="article_main_img" src="../image/cessna_citation_cj3.jpg" alt="img">
                    </div>
                    <div class="news_content p-2">
                        <h3 class="mb-0 mt-1">Cessna 150</h3>
                        <a href="planeInformation.php">Перейти</a>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-3 mb-4">
                    <div class="catalog_img">
                        <img class="article_main_img" src="../image/cesna150.jpg" alt="img">
                    </div>
                    <div class="news_content p-2">
                        <h3 class="mb-0 mt-1">Cessna 150</h3>
                        <a href="planeInformation.php">Перейти</a>
                    </div>
                </div>
                <div class="col-6 col-md-4 col-lg-3 mb-4">
                    <div class="catalog_img">
                        <img class="article_main_img" src="../image/cesna150.jpg" alt="img">
                    </div>
                    <div class="news_content p-2">
                        <h3 class="mb-0 mt-1">Cessna 150</h3>
                        <a href="planeInformation.php">Перейти</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Фильтры -->
        <div class="col-9 col-md-3 col-xl-2 order-sm-1 mb-4">
            <div class="catalog_filter p-2">
                <div class="filter_title mb-3 mt-2">
                    <h4>Фильтры</h4>
                    <button id="filter_clear">очистить</button>
                </div>
                <div class="divider"></div>
                <div class="filter">
                    <div class="mb-3">
                        <p class="mt-0 mb-1">Дальность полета</p>
                        <div class="filter_input" id="flight_range">
                            <input type="number" value="0" min="0">
                            &#8212;
                            <input type="number" value="0" min="0">
                        </div>
                    </div>
                    <div class="mb-3">
                        <p class="mt-0 mb-1">Крейсерская скорость</p>
                        <div class="filter_input" id="cruising_speed">
                            <input type="number" value="0" min="0">
                            &#8212;
                            <input type="number" value="0" min="0">
                        </div>
                    </div>
                    <div class="mb-3">
                        <p class="mt-0 mb-1">Число пассажиров</p>
                        <div class="filter_input" id="number_passengers">
                            <input type="number" value="0" min="0">
                            &#8212;
                            <input type="number" value="0" min="0">
                        </div>
                    </div>
                    <div class="mb-3">
                        <p class="mt-0 mb-1">Длина</p>
                        <div class="filter_input" id="plane_length">
                            <input type="number" value="0" min="0">
                            &#8212;
                            <input type="number" value="0" min="0">
                        </div>
                    </div>
                    <div class="mb-3">
                        <p class="mt-0 mb-1">Ширина</p>
                        <div class="filter_input" id="plane_width">
                            <input type="number" value="0" min="0">
                            &#8212;
                            <input type="number" value="0" min="0">
                        </div>
                    </div>
                    <div>
                        <button class="btn-register">Применить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="divider"></div>
</main>

<?php
    require $dir . 'components/footer.php'
?>

</body>
</html>