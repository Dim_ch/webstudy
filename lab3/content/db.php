<?php
    $title = 'БД';
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    $scripts = array();
    
    require $dir . 'controllers/dbController.php';
    
    $strDbXml = $dir . 'db.xml';
    $dbController = new dbController($strDbXml);
    $planes = $dbController->getPlanes();
    $isInfo = false;

    require $dir . 'components/header.php';
?>

<main class="container">
    <div class="row row-padding">
        <?php
            require $dir."/components/tablePlane.php";
        ?>
    </div>
</main>

<?php
    require $dir . 'components/footer.php'
?>

</body>
</html>