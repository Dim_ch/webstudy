<main class="container">
    <?php
        $scripts = array();
        $dir = $_SERVER['DOCUMENT_ROOT'].'/';
        require $dir."/components/slider.php"
    ?>

    <div class="divider"></div>
    
    <h1 class="text-center mt-4 mb-4 news_title">
        Новости
        <span class="tooltip">hello</span>
    </h1>

    <div class="row news pb-4 mt-5">
        <div class="col-12 col-xl-6 mb-4">
            <div class="news_img">
                <img src="image/cessna_history.jpg" alt="История бренда">
            </div>
            <div class="news_content">
                <h3 class="mb-0 text-center">Cessna: история бренда. Продукция частной авиации</h3>
                <div  class="news_text mb-3">
                    <p><i>Cessna Aircraft Company</i>, Textron Aviation - американский авиапроизводитель, входящий в список наиболее известных и крупных авиационных корпораций мира, занимающийся разработкой и конструированием малых и средних административных самолетов, а также спортивных самолетов и монопланов...</p>
                </div>
                <a href="content/news.php">Перейти</a>
            </div>
        </div>
        <div class="col-12 col-xl-6 mb-4">
            <div class="news_img">
                <img src="image/cessna_172.png" alt="cessna 172">
            </div>
            <div class="news_content">
                <h3 class="mb-0 text-center">Продукция деловой авиации</h3>
                <div  class="news_text mb-3">
                    <p class="mt-0">Начало производства частных самолетов пришлось на 1968 <sub>год</sub>: Америка дала понять, что богатым людям нужны комфортабельные личные самолеты...</p>
                </div>
                <a href="content/news.php">Перейти</a>
            </div>
        </div>
        <div class="col-12 col-xl-6 mb-4">
            <div class="news_img">
                <img src="image/cessna_172.png" alt="cessna 172">
            </div>
            <div class="news_content">
                <h3 class="mb-0 text-center">Продукция деловой авиации</h3>
                <div  class="news_text mb-3">
                    <p class="mt-0">Начало производства частных самолетов пришлось на 1968 год: Америка дала понять, что богатым людям нужны комфортабельные личные самолеты...</p>
                </div>
                <a href="content/news.php">Перейти</a>
            </div>
        </div>
        <div class="col-12 col-xl-6 mb-4">
            <div class="news_img">
                <img src="image/cessna_history.jpg" alt="История бренда">
            </div>
            <div class="news_content">
                <h3 class="mb-0 text-center">Cessna: история бренда. Продукция частной авиации</h3>
                <div  class="news_text mb-3">
                    <p>Cessna Aircraft Company, Textron Aviation - американский авиапроизводитель, входящий в список наиболее известных и крупных авиационных корпораций мира, занимающийся разработкой и конструированием малых и средних административных самолетов, а также спортивных самолетов и монопланов...</p>
                </div>
                <a href="content/news.php">Перейти</a>
            </div>
        </div>
    </div>
    <div class="divider"></div>
</main>