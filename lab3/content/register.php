<?php
    $title = 'Регистрация';
    $scripts = array();
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';

    $validFirstName = false;
    $validLastName = false;
    $validEmail = false;
    $validPassword1 = false;
    $validPassword2 = false;
    
    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_POST['btn_ok'])) {
            if (isset($_POST['first_name'])) {
                $validFirstName = preg_match('|^[А-Я][а-я]{2,}$|u', $_POST['first_name'], $matches,
                PREG_OFFSET_CAPTURE);
            }
            
            if (isset($_POST['last_name'])) {
                $validLastName = preg_match('|^[А-Я][а-я]{2,}$|u', $_POST['last_name'], $matches,
                PREG_OFFSET_CAPTURE);
            }
            
            if (isset($_POST['email'])) {
                $validEmail = preg_match('|^[A-Z0-9][A-Z0-9._%+-]+@[A-Z0-9-]+\.[A-Z]{2,4}$|i', $_POST['email'], $matches,
                PREG_OFFSET_CAPTURE);
            }
            
            if (isset($_POST['password1'])) {
                $validPassword1 = preg_match('|^[A-Z0-9\\\/*-+.,]{6,20}$|i', $_POST['password1'], $matches,
                PREG_OFFSET_CAPTURE);
            }
            
            if (isset($_POST['password2'])) {
                $validPassword2 = strcmp($_POST['password1'], $_POST['password2']) == 0;
            }
        }
    }
    
    require $dir . 'components/header.php';
    ?>

<main class="register">
    <form class="container row-padding register_block mt-5 mb-5" method="POST">
        <h2 class="text-center">Регистрация</h2>
        <div class="divider_footer"></div>
        <div class="row mb-4 mt-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Имя: </p>
            </div>
            <div class="col-8">
                <input type="text" name="first_name" value="<?= isset($_POST['first_name']) ? $_POST['first_name']: ""; ?>">
                <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
                <?php if (!$validFirstName): ?>
                    <p class="text-error register_hint mt-0 mb-0">
                        Имя должно состоять минимум из 3 букв,
                        содержать буквы русского алфавита и не содержать пробелов.<br>
                        Первая буква должна быть прописной.
                    </p>
                    <?php else: ?>
                    <p class="text-correct text-right mt-0 mb-0">&#10003;</p>
                    <?php endif; ?>
                <?php }?>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Фамилия: </p>
            </div>
            <div class="col-8">
                <input type="text" name="last_name" value="<?= isset($_POST['last_name']) ? $_POST['last_name']: ""; ?>">
                <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
                    <?php if (!$validLastName): ?>
                    <p class="text-error register_hint mt-0 mb-0">
                        Фамилия должна состоять минимум из 3 букв,
                        содержать буквы русского алфавита и не содержать пробелов.<br>
                        Первая буква должна быть прописной.
                    </p>
                    <?php else: ?>
                    <p class="text-correct text-right mt-0 mb-0">&#10003;</p>
                    <?php endif; ?>
                <?php }?>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Почта: </p>
            </div>
            <div class="col-8">
                <input type="text" name="email" value="<?= isset($_POST['email']) ? $_POST['email']: ""; ?>">
                <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
                    <?php if (!$validEmail): ?>
                    <p class="text-error register_hint mt-0 mb-0">
                        Введите возможный email адрес. Например, sample_adres@mail.com
                    </p>
                    <?php else: ?>
                    <p class="text-correct text-right mt-0 mb-0">&#10003;</p>
                    <?php endif; ?>
                <?php }?>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Пароль: </p>
            </div>
            <div class="col-8">
                <input type="password" name="password1" value="<?= isset($_POST['password1']) ? $_POST['password1']: ""; ?>">
                <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
                    <?php if (!$validPassword1): ?>
                    <p class="text-error mt-0 mb-0">
                        Пароль должен состоять из букв латинского алфавита, цифр, и символов "/\*-+.,".<br>
                        Первый символ пароля должен быть латинской буквой или цифрой.<br>
                        Длина пароля должна быть не меньше 6 символов.<br>
                    </p>
                    <?php else: ?>
                    <p class="text-correct text-right mt-0 mb-0">&#10003;</p>
                    <?php endif; ?>
                <?php }?>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Повторите пароль: </p>
            </div>
            <div class="col-8">
                <input type="password" name="password2" value="<?= isset($_POST['password2']) ? $_POST['password2']: ""; ?>">
                <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
                    <?php if (!$validPassword2): ?>
                    <p class="text-error register_hint mt-0 mb-0">
                        Пароли должны совпадать.
                    </p>
                    <?php else: ?>
                    <p class="text-correct text-right mt-0 mb-0">&#10003;</p>
                    <?php endif; ?>
                <?php }?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <input class="btn-register" type="submit" name="btn_ok" value="Зарегистрироваться">
            </div>
        </div>
    </form>
</main>

<?php
    require $dir . 'components/footer.php'
?>

</body>
</html>