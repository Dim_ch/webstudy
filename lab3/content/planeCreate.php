<?php
    $title = 'Добавление самолёта';
    $dir = $_SERVER['DOCUMENT_ROOT'].'/';
    $scripts = array();

    require $dir . 'controllers/dbController.php';
    
    $valueButton = "Добавить";
    $action = "/controllers/dbCreateElem.php";
    
    $strDbXml = $dir . 'db.xml';
    $dbController = new dbController($strDbXml);
    $postFlag = isset($_GET['id']);
    $plane;
    
    if($postFlag) {
        $valueButton = "Редактировать";
        $title = 'Редакитрование самолёта';
        $plane = $dbController->getPlane($_GET['id']);
        $action = "/controllers/dbEditElem.php";
    }
    
    require $dir . 'components/header.php';
?>

<main class="register">
    <form
        class="container row-padding register_block mt-5 mb-5"
        action="<?= $action ?>"
        method="POST"
        style="width: 80%">

        <h2 class="text-center">Добавление самолёта</h2>
        <div class="divider_footer"></div>
        <div class="row mb-4 mt-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Модель: </p>
            </div>
            <div class="col-8">
                <input type="text" name="name" value="<?= $postFlag ? $plane->name: ''; ?>">
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Дальность полета (км): </p>
            </div>
            <div class="col-8">
                <input type="text" name="range" value="<?= $postFlag ? $plane->range: ''; ?>">
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Крейсерская скорость (км/ч): </p>
            </div>
            <div class="col-8">
                <input type="text" name="speed" value="<?= $postFlag ? $plane->speed: ''; ?>">
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Максимальное число пассажиров: </p>
            </div>
            <div class="col-8">
                <input type="text" name="passengers" value="<?= $postFlag ? $plane->passengers: ''; ?>">
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Длина: </p>
            </div>
            <div class="col-8">
                <input type="text" name="length" value="<?= $postFlag ? $plane->length: ''; ?>">
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Ширина: </p>
            </div>
            <div class="col-8">
                <input type="text" name="width" value="<?= $postFlag ? $plane->width: ''; ?>">
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-4 text-right">
                <p class="mb-0 mt-0">Высота: </p>
            </div>
            <div class="col-8">
                <input type="text" name="height" value="<?= $postFlag ? $plane->height: ''; ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <input class="btn-register" type="submit" name="btn_ok" value="<?= $valueButton ?>">
                <?php if($postFlag): ?>
                    <input hidden name="id" value="<?= $_GET['id']?>">
                <?php endif; ?> 
            </div>
        </div>
    </form>
</main>

<?php
    require $dir . 'components/footer.php'
?>

</body>
</html>