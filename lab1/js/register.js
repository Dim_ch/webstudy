let validFirstName = false;
let validLastName = false;
let validEmail = false;
let validPassword1 = false;
let validPassword2 = false;

// Показывает/скрывает информацию
function showInformation(flag, parent) {
    if (flag) showCorrect(parent);
    else showError(parent);
}

// Функция валидации имени
function validateName(event) {
    let value = event.target.value;
    let regexp = /^[А-Я][а-я]{2,}$/;
    let result = regexp.test(value);

    showInformation(result, event.target.parentElement);

    return result;
}

// Функция валидации emil
function validateEmail(event) {
    let value = event.target.value;
    let regexp = /^[A-Z0-9][A-Z0-9._%+-]+@[A-Z0-9-]+\.[A-Z]{2,4}$/i;
    let result = regexp.test(value);

    showInformation(result, event.target.parentElement);

    validEmail = result;
}

// Функция валидации пароля1
function validatePassword1(event) {
    let value = event.target.value;
    let regexp = /^[A-Z0-9\\\/*-+.,]{6,20}$/i;
    let result = regexp.test(value);

    showInformation(result, event.target.parentElement);

    validPassword1 = result;
}

// Функция валидации пароля
function validatePassword2(event) {
    let value = event.target.value;
    let password1 = document.querySelector('form.register_block input[name="password1"]')
                            .value;
    let result = value === password1

    showInformation(result, event.target.parentElement);

    validPassword2 = result;
}

// Отображает сообщения с ошибками и скрывает галочку
function showError(parent) {
    let errorElement = parent.querySelectorAll('p[class*="text-error"]');
    let corretElement = parent.querySelectorAll('p[class*="text-correct"]');

    // Выбирает классы без hidden
    for (item of errorElement) {
        let newClassName = item.className.split(' ').filter(x => x !== 'hidden');

        item.className = newClassName.join(' ');
    }

    // Выбирает классы без inline-block & hidden добавляет hidden
    // и записывает новый класс
    for (item of corretElement) {
        let newClassName = item.className.split(' ')
            .filter(x => x !== 'hidden');

        newClassName.push('hidden');

        item.className = newClassName.join(' ');
    }
}

// Обратная showError
function showCorrect(parent) {
    let errorElement = parent.querySelectorAll('p[class*="text-error"]');
    let corretElement = parent.querySelectorAll('p[class*="text-correct"]');

    // Выбирает классы без hidden, добавляет hidden и записывает новый класс
    for (item of errorElement) {
        let newClassName = item.className.split(' ')
            .filter(x => x !== 'hidden');

        newClassName.push('hidden');

        item.className = newClassName.join(' ');
    }

    // Выбирает классы без inline-block & hidden добавляет inline-block
    // и записывает новый класс
    for (item of corretElement) {
        let newClassName = item.className.split(' ')
            .filter(x => x !== 'hidden');

        item.className = newClassName.join(' ');
    }
}

document.querySelector('form.register_block input[name="first_name"]')
    .addEventListener('input', (event) => validFirstName = validateName(event));

document.querySelector('form.register_block input[name="last_name"]')
    .addEventListener('input', (event) => validLastName = validateName(event));

document.querySelector('form.register_block input[name="email"]')
    .addEventListener('input', validateEmail);

document.querySelector('form.register_block input[name="password1"]')
    .addEventListener('input', validatePassword1);

document.querySelector('form.register_block input[name="password2"]')
    .addEventListener('input', validatePassword2);

document.querySelector('form.register_block button[type="submit"]')
    .addEventListener('click', function(event) {
        event.preventDefault();
        
        if (validFirstName && validLastName && validEmail
            && validPassword1 && validPassword2)
            alert('Все поля валидны');
        else
        alert('Не все поля валидны');
    });